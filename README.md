# 部署系统说明

## 一、WEB端使用手册

### 功能概述

- 查看环境列表
- 查看指定环境下服务以及服务的实例数
- 重启指定服务
- 删除指定服务

### 功能详解

#### 查看环境列表

- ![image-20210610145340201](./images/查看环境.png)
- 进入网页，点击查看所有环境名，在下方展示出现有的环境名

#### 查看指定环境下的服务

- ![image-20210610145551025](./images/查看指定环境下的服务.png)
- 点击查看已部署服务按钮，在搜索框中输入环境名敲回车，下方展示当前环境中的服务和实例数

#### 重启服务

- ![image-20210610145947524](./images/重启单个服务.png)
- 点击服务名同行的重启服务按钮，可重启该服务

#### 删除服务

- ![image-20210610150049173](./images/删除服务.png)
- 点击服务名同行的删除服务按钮，可删除该服务
- 点击删除所有服务按钮，可删除该环境下所有服务



## 二、本系统的部署

### 名称解释

- 环境：用于部署服务的docker swarm集群

### 1.修改系统配置参数

如有需求可修改以下参数

#### 文件参数

系统依赖的文件名称与路径位于com.lenovodevops.deployment.common.consts.ConfigConsts类中

#### docker属性参数

docker swarm中stack的属性位于com.lenovodevops.deployment.common.consts.DockerConsts类中

#### 时间参数

部署时间有关参数位于com.lenovodevops.deployment.common.consts.TimeConst类中

### 2.修改清单文件list.txt

清单示例：

```shell
hello|https://gitlab.com/zhanghaozh/jenkins1.git|master|0|
Jenkins|https://gitlab.com/zhanghaozh/dockerfile.git|master|1|
more|https://gitlab.com/zhanghaozh/more.git|master|0|
gennies|https://gitee.com/UserWD/mutimodule.git|master|1|module1
```

根据需求修改清单内容

### 3.修改自定义yaml文件services.yml

根据services.yml中的提示修改内容，完成对某些服务的yaml文件自定义

可在此定义redis密码

### 4.修改Dockerfile

如果在第1步中**修改过文件参数**，请修改Dockerfile相关内容

```dockerfile
FROM openjdk:8-jdk-alpine
LABEL MAINTAINER="lenovodevops"
# 如果修改了文件参数 请修改相关的建立路径与文件的命令 到注释线为止
# 与ENVIRONMENTS_CONFIG_PATH_NAME和CUSTOM_YAML_PATH_NAME相关
RUN mkdir /home/longStorage 
# 与ENVIRONMENTS_CONFIG_PATH_NAME相关
RUN touch /home/longStorage/environments.config
# 与LOG_PATH_NAME相关
RUN mkdir /home/logStorage
# 与YAML_FILE_PATH相关
RUN mkdir /home/yamlStorage
# 与LIST_FILE_PATH相关
RUN mkdir /home/listStorage
# 与LIST_FILE_PATH相关
COPY list.txt /home/listStorage
# 与CUSTOM_YAML_PATH_NAME相关
COPY services.yml /home/longStorage
# 与AUTHORITY_KEY_PATH相关
RUN mkdir /root/.ssh/
COPY id_rsa /root/.ssh/
COPY id_rsa.pub /root/.ssh/
RUN chmod 755 /root/.ssh/
RUN chmod 600 /root/.ssh/id_rsa
################################################
ARG JAR_FILE
COPY ${JAR_FILE} deployment.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/deployment.jar"]
```

### 5.打包、生成镜像、部署

该步骤由jenkins部分完成

### 6.修改免密登录密钥对时注意

可修改./id_rsa与./id_rsa.pub来更新免密登录密钥

本系统使用了jsch包，jsch是一个ssh协议的java实现。

但是由于jsch在2018年11月后没有再更新，可能不支持现版本ssh-keygen生成密钥的加密方式。

可以使用ssh-keygen -t 指定加密方式，系统默认的密钥对通过ssh-keygen -t rsa生成

### 7.系统日志

可以通过系统日志来查看docker集群对于部署过程及结果的感应。

查看日志命令：

​	docker logs 容器id



## 三、目标环境的搭建

### 名称解释

- 目标环境：用于部署本系统部署的服务的docker swarm集群。一个环境对应一个ip。本系统认为各环境间是**物理隔离**的。即各环境中docker stack名称相同不存在相互影响，服务名相同也不存在相互影响

### 1. 安装docker

docker版本要求19.03.0 及以上（由于我们建立Docker swarm集群时的compose文件指定版本为 version 3.9, 而与之相对应的docker版本则也需进行提升）

```shell
# 在centos环境下执行此命令可以查看当前提供的所有docker版本，然后选择指定版本安装即可
yum list docker-ce --showduplicates | sort -r
```

### 2. 建立docker swarm集群

- 环境中需要有一个稳定的manager结点，接收本系统的远程命令**在本文中简称这个稳定的manager结点**

**为** ***manager***，以下步骤在manager上执行

### 3. 建立docker网络

```shell
# 在manager上执行以下命令
docker network create --driver overlay  --attachable devopsnet
```

- 如果修改过docker属性参数**DOCKER_NETWORK** 请建立相应名字的docker网络

### 4. 创建yaml存储目录

```shell
# 在manager上执行
mkdir /home/yaml
```

- 如果在本系统的部署时**修改过文件参数** ***YAML_FILE_TARGET_PATH*** 请建立相应的路径

### 5.SSH免密登录准备

- 在manager上执行脚本authorized_key.sh

```shell 
./authorized_key.sh
```

