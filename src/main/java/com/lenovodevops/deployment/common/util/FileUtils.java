package com.lenovodevops.deployment.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    
    /**
     * @author 满显凡
     * @param content
     * @param path
     * @param append true表示在文件中追加
     */
    public static void saveAsFile(String content, String path, Boolean append) throws IOException {
        FileWriter fwriter = null;
        try {
            File file = new File(path);
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    throw new IOException("create" + path + "fail");
                }
            }
            // true表示不覆盖原来的内容，而是加到文件的后面。若要覆盖原来的内容，直接省略这个参数就好
            fwriter = new FileWriter(file, append);
            fwriter.write(content);
        } catch (IOException ex) {
            throw ex;
        } finally {
            try {
                fwriter.flush();
                fwriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * @author 满显凡
     * @param path
     */
    public static List<String> readFile(String path) {
        File file = new File(path);
        BufferedReader reader = null;
        List<String> content = new ArrayList<String>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                content.add(tempString);
            }
            return content;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {}
            }
        }
    }

    /**
     * @author 满显凡
     * @param file
     */
    public static void deleteFile(File file) {
        if (file.isDirectory()) {
            //递归删除文件夹下所有文件
            File[] files = file.listFiles();
            for (File f : files) {
                deleteFile(f);
            }
            //删除文件夹自己
            if (file.listFiles().length == 0) {
                file.delete();
            } 
        } else {
            // 如果是文件,就直接删除自己
            file.delete();
        }
      }
    
}
