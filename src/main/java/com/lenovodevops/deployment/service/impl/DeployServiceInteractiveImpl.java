package com.lenovodevops.deployment.service.impl;

import com.lenovodevops.deployment.common.consts.TimeConsts;
import com.lenovodevops.deployment.common.util.CacheManagerUtils;
import com.lenovodevops.deployment.common.util.IPUtils;
import com.lenovodevops.deployment.model.bo.DeploymentBO;
import com.lenovodevops.deployment.model.bo.DeploymentStatusBO;
import com.lenovodevops.deployment.model.exception.ParamIllegalException;
import com.lenovodevops.deployment.service.DeployService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 与用户有更多交互的部署方法
 * @author 满显凡
 */
@Slf4j
@Service("DeployServiceInteractive")
public class DeployServiceInteractiveImpl implements DeployService {

    @Autowired
    DeployServiceInteractiveSlave slave;

    /**
     * @author 满显凡
     * @param param test|192.168.0.1|1623202466|test:v1
     */
    @Override
    public String deployImages(String param) {
        DeploymentBO deploymentBO;
        try {
            deploymentBO = getEnvironmentsFromParam(param);
        } catch (ParamIllegalException e) {
            log.error(e.getMessage(), e);
            return e.getMessage();
        }
        
        // 获取当前
        CacheManagerUtils cacheManagerUtils =  CacheManagerUtils.getInstance();
        Object obj = cacheManagerUtils.get(deploymentBO.getID());

        // 如果该id没有被部署
        if (obj == null) {
            // 正在部署的内容
            DeploymentStatusBO deploymentStatus = new DeploymentStatusBO(deploymentBO.getID(),
                DeploymentStatusBO.CODE_DEPLOYING,
                DeployService.DEPLOYING);

            // 记录正在部署 
            cacheManagerUtils.put(deploymentBO.getID(), deploymentStatus, TimeConsts.TIME_OUT);
            // 部署
            log.info("开始一次新的部署");
            slave.deploy(deploymentBO);
            // 返回正则部署
            return "0\n" + deploymentStatus.toString();
        
        // 如果该id被部署了
        } else {
            DeploymentStatusBO status = (DeploymentStatusBO)obj;
            // 状态为ok error deploying时直接返回结果 不再进行检查
            if (DeploymentStatusBO.CODE_ERROR.equals(status.getCode())
                || DeploymentStatusBO.CODE_OK.equals(status.getCode())) {
                String exit = "1\n";
                return exit + status.toString();
            } else if (DeploymentStatusBO.CODE_DEPLOYING.equals(status.getCode())) {
                String exit = "0\n";
                return exit + status.toString();
            // 其他状态 done uncertain时进行检测
            } else {
                DeploymentStatusBO newStatus = slave.checkRlt(deploymentBO);
                // 如果检查结果为ok error 则将部署状态更新
                cacheManagerUtils.put(deploymentBO.getID(), newStatus, TimeConsts.TIME_OUT);
                String exit = "0\n";
                // 如果检测结果为终止状态 exit置为1
                if (DeploymentStatusBO.CODE_OK.equals(newStatus.getCode()) || 
                    DeploymentStatusBO.CODE_ERROR.equals(newStatus.getCode())) {
                        exit = "1\n";
                }
                // 如果原状态是done则返回done的信息
                if (DeploymentStatusBO.CODE_DONE.equals(status.getCode())) {
                    return exit + status.toString() + "\n\n" + newStatus.toString();
                }
                return exit + newStatus.toString();
            }
        }
   
    }

     /**
     * 将字符串参数转化为EnvironmentBO列表
     * @author 满显凡
     * @param param test|192.168.0.1|1623202466|test:v1
     * @return
     */
    
    private DeploymentBO getEnvironmentsFromParam(String param) throws ParamIllegalException {
        // 参数检查
        if (param.contains("\r\n")) {
            throw new ParamIllegalException("参数应为一行");
        }
        String[] params = param.split("\\|");
        if (params.length < DeployService.PARAM_MIN_LENGTH) {
            throw new ParamIllegalException("缺少字段");
        }
        if (params[0].length() < 1) {
            throw new ParamIllegalException("缺失环境名");
        }
        if (params[1].length() > 0) {
            // ip要符合格式
            if (!IPUtils.isIp(params[1])) {
                throw new ParamIllegalException("ip非法");
            } 
        }
        if (params[2].length() < 1) {
            throw new ParamIllegalException("缺失时间戳");
        }
        
        DeploymentBO deployment = new DeploymentBO();
        deployment.setEnvironmentName(params[0]);
        deployment.setIp(params[1]);
        deployment.setCreateTime(Long.valueOf(params[2]));

        for (int i = 3; i < params.length; i++) {
            // 确保image中带有版本号 
            String[] image = params[i].split("/");
            String[] nameAndTage = image[image.length - 1].split(":");
            if (nameAndTage.length != 2) {
                throw new ParamIllegalException("image非法");
            }
            deployment.addImages(params[i]);
        }
        return deployment;
    }
    
}
