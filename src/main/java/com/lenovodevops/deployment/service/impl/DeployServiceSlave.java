package com.lenovodevops.deployment.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.consts.TimeConsts;
import com.lenovodevops.deployment.common.consts.DockerConsts;
import com.lenovodevops.deployment.common.enums.ResponseStatus;
import com.lenovodevops.deployment.common.util.CacheManagerUtils;
import com.lenovodevops.deployment.common.util.FileUtils;
import com.lenovodevops.deployment.common.util.YamlUtils;
import com.lenovodevops.deployment.db.EnvironmentConfig;
import com.lenovodevops.deployment.db.Medis;
import com.lenovodevops.deployment.model.bo.CommandRltBO;
import com.lenovodevops.deployment.model.bo.DeploymentBO;
import com.lenovodevops.deployment.model.bo.DeploymentCheckBO;
import com.lenovodevops.deployment.model.bo.DeploymentRlt;
import com.lenovodevops.deployment.model.bo.ServiceStatusBO;
import com.lenovodevops.deployment.model.exception.FileUploadException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;
import com.lenovodevops.deployment.service.DeployService;
import com.lenovodevops.deployment.service.DockerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class DeployServiceSlave {

    @Autowired
    @Qualifier("DockerServiceImpl")
    private DockerService dockerService;

    final private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 正式在新线程中开启部署任务
     * @author 满显凡
     */
    @Async
    public void deployImageSlave(String param, DeploymentBO deployment) {
        Long processStartTime = System.currentTimeMillis();

        // 调用部署方法 获取结果 
        DeploymentRlt rlt = this.deployImagesGetRlt(deployment);
        rlt.setProcessStartTime(processStartTime);
        rlt.setProcessEndTime(System.currentTimeMillis());
        // 将简要结果写入缓存 
        CacheManagerUtils cacheManagerUtils = CacheManagerUtils.getInstance();
        cacheManagerUtils.put(param, rlt, TimeConsts.TIME_OUT);
        try {
            // 将全部结果写入日志 
            String log = rlt.toString() + '\n';
            FileUtils.saveAsFile(log, ConfigConsts.LOG_PATH_NAME, true);
        } catch(IOException e) {
            e.printStackTrace();
            logger.error("log save error", e);
        }
    }

    /**
     * 部署服务并返回结果
     * @author 满显凡
     * @param param
     * @return
     */
    private DeploymentRlt deployImagesGetRlt(DeploymentBO deployment) {
        EnvironmentConfig environmentConfig;
        // 如果没有提供ip
        if (deployment.getIp() == null) {
            if (!Medis.containsEnvironment(deployment.getEnvironmentName())) {
                return new DeploymentRlt(ResponseStatus.ENVIRONMENT_UNKNOW.getMsg(), deployment.getID(), deployment.getEnvironmentName());
            }
            // 获取环境信息 
            environmentConfig = Medis.getEnvironmentConfigByName(deployment.getEnvironmentName());
        // 提供了ip
        } else {
            // 获取环境信息 
            environmentConfig = Medis.getEnvironmentConfigByName(deployment.getEnvironmentName());
            // 没有记录该环境 或者已经记录环境但是ip发生改变
            if (environmentConfig == null || !environmentConfig.getIp().equals(deployment.getIp())) {
                // 新建并记录环境
                environmentConfig = new EnvironmentConfig(deployment.getEnvironmentName(), deployment.getIp());
                try {
                    Medis.InsertEnvironment(environmentConfig);
                } catch (IOException e) {
                    DeploymentRlt rlt = new DeploymentRlt(DeployService.PRE_ERROR, deployment.getID(), deployment.getEnvironmentName());
                    rlt.setErr(e.getMessage());
                    return rlt;
                }
            }
        }
        // 生成用于部署的yaml 
        Map<Object,Object> yamlMap = YamlUtils.createYamlMap(deployment.getImages());
        String yamlName = deployment.getID() + ".yml";

        // 开始多次尝试写入yaml文件
        DeploymentRlt deploymentRlt = null;
        for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
            try {
                deploymentRlt = null;
                // 把yaml写入文件 
                YamlUtils.createYaml(yamlMap, yamlName);
                break;
            } catch(IOException e) {
                deploymentRlt = new DeploymentRlt(DeployService.PRE_ERROR, deployment.getID(), deployment.getEnvironmentName());
                deploymentRlt.setErr(e.getMessage());
            }
        }
        if (deploymentRlt != null) {
            return deploymentRlt;
        }

        // 开始多次尝试上传yaml文件
        for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
            try {
                deploymentRlt = null;
                // 把yaml上传到指定主机 
                dockerService.uploadFile(environmentConfig, yamlName);
            } catch(FileUploadException e) { // 文件上传出错 
                deploymentRlt = new DeploymentRlt(DeployService.PRE_ERROR, deployment.getID(), deployment.getEnvironmentName());
                deploymentRlt.setErr(e.getMessage());
            }
        }
        if (deploymentRlt != null) {
            return deploymentRlt;
        }

        Long deployStartTime = System.currentTimeMillis();
        Long deployEndTime = 0L;
        // 最多部署2次 
        for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
            try {
                // 执行部署命令 
                CommandRltBO commandRlt = dockerService.deployStack(environmentConfig, yamlName, DockerConsts.STACK_NAME);
                deployEndTime = System.currentTimeMillis();
                // 如果执行有问题（不一定不成功） 
                if (!commandRlt.isSuccess()) {
                    deploymentRlt = new DeploymentRlt(DeployService.EXEC_ERROR, deployment.getID(), deployment.getEnvironmentName(), commandRlt);
                }
                
                // 检查服务启动情况 
                DeploymentCheckBO deploymentCheckBO = this.getDeployRlt(environmentConfig, deployment);
                if (deploymentCheckBO.getRlt().equals(DeploymentCheckBO.RLT_ERR)) {
                    // 不能确定结果 
                    if (commandRlt.isSuccess()) {
                        deploymentRlt = new DeploymentRlt(DeployService.CAN_NOT_DETERMINE, deployment.getID(), deployment.getEnvironmentName());
                    }
                    continue;
                } else if (deploymentCheckBO.getRlt().equals(DeploymentCheckBO.RLT_UNKNOW)) {
                    // 有无实例服务 
                    if (commandRlt.isSuccess()) {
                        deploymentRlt = new DeploymentRlt(DeployService.UNCERTAIN_SERVICE, deployment.getID(), deployment.getEnvironmentName()); 
                    }
                    deploymentRlt.setServices(deploymentCheckBO.getUncertainsString());
                    continue;
                } else if (deploymentCheckBO.getRlt().equals(DeploymentCheckBO.RLT_DEFECT)) {
                    continue;
                }
                deploymentRlt = new DeploymentRlt(deployment.getID(), deployment.getEnvironmentName(), commandRlt);
                break;
            // 命令未成功发送到目标主机或没有成功收到回复 都视为失败，最终移除stack 
            } catch(RemoteExecuteException|IOException e) { 
                deployEndTime = System.currentTimeMillis();
                deploymentRlt = new DeploymentRlt(e.getMessage(), deployment.getID(), deployment.getEnvironmentName());
                e.printStackTrace();
            }
        }
        deploymentRlt.setDeployStartTime(deployStartTime);
        deploymentRlt.setDeployEndTime(deployEndTime);
        return deploymentRlt;
    }

    /**
     * @author 满显凡
     * @param deploymentBO
     * @return
     * @throws IOException
     * @throws RemoteExecuteException
     */
    private DeploymentCheckBO getDeployRlt(EnvironmentConfig environmentConfig, DeploymentBO deploymentBO) throws RemoteExecuteException, IOException {
        CommandRltBO commandRltBO;
        // 获取检查结果 
        commandRltBO = dockerService.listServices(environmentConfig, DockerConsts.STACK_NAME);
        // 获取失败 
        if (!commandRltBO.isSuccess()) {
            return new DeploymentCheckBO(DeploymentCheckBO.RLT_ERR);
        }
        DeploymentCheckBO deploymentCheckBO = new DeploymentCheckBO();
        List<ServiceStatusBO> uncertains = new ArrayList<ServiceStatusBO>();
        // 获取服务 
        List<ServiceStatusBO> services = new ArrayList<ServiceStatusBO>();
        for (int i = 1; i < commandRltBO.getOk().size(); ++i) {
            String[] rlt = commandRltBO.getOk().get(i).split(" +");
            ServiceStatusBO serviceStatusBO = new ServiceStatusBO();
            serviceStatusBO.setService(rlt[1].substring(DockerConsts.STACK_NAME.length() + 1));
            serviceStatusBO.setReplicas(rlt[3]);
            services.add(serviceStatusBO);
        }
        // 检查服务是否有实例 
        Integer shoot = 0;
        for (String image: deploymentBO.getImages()) {
            String[] rImage = image.split("/");
            String[] nameAndTag = rImage[rImage.length - 1].split(":");
            image = nameAndTag[0];
            for (ServiceStatusBO s: services) {
                if (image.equals(s.getService())) {
                    ++shoot;
                    if (s.getReplicas().charAt(0) == '0') {
                        uncertains.add(s);
                    }
                }
            }
        }
        // 服务部署缺失
        if (shoot < deploymentBO.getImages().size()) {
            deploymentCheckBO.setRlt(DeploymentCheckBO.RLT_DEFECT);
            return deploymentCheckBO;
        }
        if (uncertains.size() > 0) {
            deploymentCheckBO.setRlt(DeploymentCheckBO.RLT_UNKNOW);
            deploymentCheckBO.setUncertains(uncertains);
        } else {
            deploymentCheckBO.setRlt(DeploymentCheckBO.RLT_OK);
        }
        return deploymentCheckBO;
    }

}
