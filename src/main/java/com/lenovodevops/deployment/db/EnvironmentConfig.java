package com.lenovodevops.deployment.db;

import lombok.Getter;
import lombok.Setter;

/**
 * 存储一个环境的名称，ip，端口，用户，密码，私钥
 * @author 满显凡
 */
@Getter
@Setter
public class EnvironmentConfig {
    
    private String name;
    private String ip;

    public EnvironmentConfig(String name, String ip) {
        this.setName(name);
        this.setIp(ip);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(this.name);
        sb.append("|");
        sb.append(this.ip);
        return sb.toString();
    }

}