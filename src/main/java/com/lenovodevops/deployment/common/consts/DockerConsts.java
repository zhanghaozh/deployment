package com.lenovodevops.deployment.common.consts;

public class DockerConsts {

    /** 
     * docker集群中堆栈的名字 
     */
    final public static String STACK_NAME = "devops";

    /**
     * docker网络名字
     */
    final public static String DOCKER_NETWORK = "devopsnet";
    
}
