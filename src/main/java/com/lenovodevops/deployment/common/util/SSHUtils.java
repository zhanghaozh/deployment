package com.lenovodevops.deployment.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.enums.ResponseStatus;
import com.lenovodevops.deployment.db.EnvironmentConfig;
import com.lenovodevops.deployment.model.exception.FileUploadException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;

import lombok.extern.slf4j.Slf4j;

/**
 * 通过jsch实现ssh连接控制服务器
 */
@Slf4j
public class SSHUtils {
    
    /** 命令返回结果正确执行结果与错误的分界线 */
    final public static String execExtLine = "#####EXEC#EXT#####";

    /** 目标用户 */
    final public static String root = "root";

    /** 目标端口 */
    final public static int port = 22;

    /**
     * @author 满显凡
     * @param command
     * @param en
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    public static List<String> remoteExecute(String command, EnvironmentConfig en) throws RemoteExecuteException, IOException {
        Channel channel = null;
        try{
            JSch jSch = new JSch();
            jSch.addIdentity(ConfigConsts.AUTHORITY_KEY_PATH);  
            Session session = jSch.getSession(SSHUtils.root, en.getIp(), SSHUtils.port);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(5000);
            channel = session.openChannel("exec");
            ((ChannelExec)channel).setCommand(command);   
            channel.connect();
            List<String> rlt = new ArrayList<String>();
            InputStream input = channel.getInputStream();
            InputStream eInput = channel.getExtInputStream();
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(input));
            String inputLine = null;
            while((inputLine = inputReader.readLine()) != null) {
                rlt.add(inputLine);
            }
            rlt.add(SSHUtils.execExtLine);
            inputReader = new BufferedReader(new InputStreamReader(eInput));
            inputLine = null;
            while((inputLine = inputReader.readLine()) != null) {
                rlt.add(inputLine);
            }
            return rlt; 
        } catch(JSchException e){
            log.error(e.getMessage(), e);
            throw new RemoteExecuteException(ResponseStatus.SEND_COMMAND_FAILD, e.getMessage());
        }finally {
            if (channel != null) {
                try {
                    channel.disconnect();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

    }

    /**
     * @author 满显凡
     * @param en
     * @param basePath
     * @param fileName
     * @param targetPath
     * @throws FileUploadException
     */
    public static void uploadFile(EnvironmentConfig en, String basePath, String fileName, String targetPath) throws FileUploadException {
        ChannelSftp channel = null;
        try{
            File file = new File(basePath + fileName);  
		    InputStream is = new FileInputStream(file);

            JSch jSch = new JSch();
            jSch.addIdentity(ConfigConsts.AUTHORITY_KEY_PATH);    
            Session session = jSch.getSession(SSHUtils.root, en.getIp(), SSHUtils.port);
            //session.setPassword(en.getPassword());
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(5000);
            channel = (ChannelSftp)session.openChannel("sftp");   
            channel.connect();
            try {
                channel.cd(targetPath);
            } catch (SftpException e) {
                channel.mkdir(targetPath);
                channel.cd(targetPath);
            }
            channel.put(is, fileName);
        } catch(Exception e){
            log.error(e.getMessage(), e);
            throw new FileUploadException(ResponseStatus.FILE_UPLOAD_FAIL, e.getMessage());
        }finally {
            if (channel != null) {
                try {
                    channel.disconnect();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }
    
}
