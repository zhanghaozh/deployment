package com.lenovodevops.deployment.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.lenovodevops.deployment.common.util.CacheManagerUtils;
import com.lenovodevops.deployment.model.bo.DeploymentBO;
import com.lenovodevops.deployment.model.bo.DeploymentRlt;
import com.lenovodevops.deployment.service.DeployService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("DeployServiceImpl")
public class DeployServiceImpl implements DeployService {

    @Autowired
    private DeployServiceSlave dockerServiceSlave;

    /**
     * @author 满显凡
     * @param param
     */
    @Override
    public String deployImages(String param) {
        if (param.contains("\r\n")) {
            String rlt = "err \\r \\n: " + param;
            return rlt;
        }
        CacheManagerUtils cacheManagerUtils =  CacheManagerUtils.getInstance();
        Object rlt = cacheManagerUtils.get(param);
        if (rlt == null) {
            // 处理参数 
            List<DeploymentBO> deployments = getEnvironmentsFromParam(param);
            // 如果参数格式错误返回 
            if (deployments == null) {
                return DeployService.PARAM_ERROR;
            }
            DeploymentBO deployment = deployments.get(0);
            // 记录正在部署 
            cacheManagerUtils.put(param, DeployService.DEPLOYING);
            // 部署 
            dockerServiceSlave.deployImageSlave(param, deployment);
            return DeployService.DEPLOYING;
        } else {
            if (rlt instanceof String) {
                return DeployService.DEPLOYING;
            } else if (rlt instanceof DeploymentRlt) {
                if (DeployService.DEPLOYMENT_RLT_OK.equals(((DeploymentRlt)rlt).getRlt())) {
                    cacheManagerUtils.remove(param);
                    return ((DeploymentRlt)rlt).getRlt();
                } else {
                    cacheManagerUtils.remove(param);
                    return ((DeploymentRlt)rlt).toSimpleString();
                }
            } else {
                return DeployService.UNKNOW_ERROR + "com.lenovodevops.deployment.service.DeployService.deployImages()";
            }
        }
        
    }

     /**
     * 将字符串参数转化为EnvironmentBO列表
     * @author 满显凡
     * @param params
     * @return
     */
    private List<DeploymentBO> getEnvironmentsFromParam(String params) {
        // 原设计用%分隔多个部署，但是部署一次只有一个环境的部署，所以该分割其实无效，返回的list长度永为1
        String[] environmentStrs = params.split("%");
        List<DeploymentBO> deployments = new ArrayList<DeploymentBO>();
        for (String e: environmentStrs) {
            String[] param = e.split("\\|");
            if (param.length < 3) {
                return null;
            }
            DeploymentBO deployment = new DeploymentBO();
            // 创建时间用于生成该次部署的id 
            deployment.setCreateTime(System.currentTimeMillis());
            deployment.setEnvironmentName(param[0]);
            deployment.setIp(param[1]);
            for (int i = 2; i < param.length; i++) {
                // 确保image中带有版本号 
                String[] image = param[i].split("/");
                String[] nameAndTage = image[image.length - 1].split(":");
                if (nameAndTage.length != 2) {
                    return null;
                }
                deployment.addImages(param[i]);
            }
            deployments.add(deployment);
        }
        return deployments;
    }

    
}
