package com.lenovodevops.deployment.service.impl;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.lenovodevops.deployment.common.aspect.LogAspect;
import com.lenovodevops.deployment.common.consts.TimeConsts;
import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.consts.DockerConsts;
import com.lenovodevops.deployment.common.enums.ResponseStatus;
import com.lenovodevops.deployment.common.util.CacheManagerUtils;
import com.lenovodevops.deployment.common.util.YamlUtils;
import com.lenovodevops.deployment.db.EnvironmentConfig;
import com.lenovodevops.deployment.db.Medis;
import com.lenovodevops.deployment.model.bo.CommandRltBO;
import com.lenovodevops.deployment.model.bo.DeploymentBO;
import com.lenovodevops.deployment.model.bo.DeploymentCheckBO;
import com.lenovodevops.deployment.model.bo.DeploymentStatusBO;
import com.lenovodevops.deployment.model.exception.FileUploadException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;
import com.lenovodevops.deployment.service.DeployService;
import com.lenovodevops.deployment.service.DockerService;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class DeployServiceInteractiveSlave {
    
    @Autowired
    DockerService dockerService;
    
    /**
     * 开始一次部署
     * @author 满显凡
     */
    @Async
    public void deploy(DeploymentBO deploymentBO) {
        Long processStartTime = System.currentTimeMillis();
        // 调用部署方法 获取结果 
        DeploymentStatusBO rlt = this.deployProcess(deploymentBO);

        rlt.setProcessStartTime(processStartTime);
        rlt.setProcessEndTime(System.currentTimeMillis());
        // 将简要结果写入缓存 
        CacheManagerUtils cacheManagerUtils = CacheManagerUtils.getInstance();
        cacheManagerUtils.put(deploymentBO.getID(), rlt, TimeConsts.TIME_OUT);
        // 将全部结果写入日志 
        String logContent = rlt.toLogString();
        log.info(logContent);
        MDC.remove(LogAspect.requestId);
    }
    
    /**
     * 执行部署前准备和部署 
     * @author 满显凡
     * @param deploymentBO
     * @return
     */
    private DeploymentStatusBO deployProcess(DeploymentBO deploymentBO) {
        EnvironmentConfig environmentConfig;
        DeploymentStatusBO rlt = null;
        boolean updateEnvironment = false;
        log.info("处理环境信息");
        // 如果没有提供ip
        if (deploymentBO.getIp() == null || deploymentBO.getIp().length() == 0) {
            if (!Medis.containsEnvironment(deploymentBO.getEnvironmentName())) {
                return new DeploymentStatusBO(deploymentBO.getID(), 
                                            DeploymentStatusBO.CODE_ERROR, 
                                            ResponseStatus.ENVIRONMENT_UNKNOW.getMsg(), 
                                            deploymentBO.getEnvironmentName(), 
                                            "null", 
                                            null);
            }
            // 获取环境信息 
            environmentConfig = Medis.getEnvironmentConfigByName(deploymentBO.getEnvironmentName());
        // 提供了ip
        } else {
            // 获取环境信息 
            environmentConfig = Medis.getEnvironmentConfigByName(deploymentBO.getEnvironmentName());
            // 没有记录该环境 或者已经记录环境但是ip发生改变
            if (environmentConfig == null || !environmentConfig.getIp().equals(deploymentBO.getIp())) {
                // 新建并记录环境
                environmentConfig = new EnvironmentConfig(deploymentBO.getEnvironmentName(), deploymentBO.getIp());
                // 记录环境 多次尝试
                updateEnvironment = true;
            }
        }

        log.info("生成yaml文件");
        // 生成用于部署的yaml 
        Map<Object,Object> yamlMap = YamlUtils.createYamlMap(deploymentBO.getImages());
        String yamlName = deploymentBO.getYamlName();
        // 开始多次尝试写入yaml文件
        for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
            try {
                rlt = null;
                // 把yaml写入文件 
                YamlUtils.createYaml(yamlMap, yamlName);
                break;
            } catch(IOException e) {
                log.error(String.format("第%d次尝试写入yaml文件失败", counter), e);
                rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                            DeploymentStatusBO.CODE_ERROR, 
                                            DeployService.PRE_ERROR, 
                                            deploymentBO.getEnvironmentName(), 
                                            deploymentBO.getIp(), 
                                            e.getMessage());
            }
        }
        if (rlt != null) {
            return rlt;
        }

        log.info("上传yaml文件");
        // 开始多次尝试上传yaml文件
        for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
            try {
                rlt = null;
                // 把yaml上传到指定主机 
                dockerService.uploadFile(environmentConfig, yamlName);
                break;
            } catch(FileUploadException e) { // 文件上传出错 
                log.error(String.format("第%d次尝试上传yaml文件失败", counter), e);
                rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                            DeploymentStatusBO.CODE_ERROR, 
                                            DeployService.PRE_ERROR, 
                                            deploymentBO.getEnvironmentName(), 
                                            deploymentBO.getIp(), 
                                            e.getMessage());
                // 不成功等待一会重试
                try {
                    Thread.sleep(TimeConsts.WAIT_TIME);
                } catch (InterruptedException ie) {
                    log.error(ie.getMessage(), ie);
                }
            }
        }
        if (rlt != null) {
            return rlt;
        }

        Long deployStartTime = System.currentTimeMillis();
        Long deployEndTime = 0L;
        log.info("开始发布部署命令");
        // 最多部署2次 
        for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
            rlt = null;
            try {
                // 执行部署命令 
                CommandRltBO commandRlt = dockerService.deployStack(environmentConfig, yamlName, DockerConsts.STACK_NAME);
                deployEndTime = System.currentTimeMillis();
                rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                            DeploymentStatusBO.CODE_DONE, 
                                            DeployService.DONE, 
                                            deploymentBO.getEnvironmentName(), 
                                            deploymentBO.getIp(), 
                                            commandRlt.getInfo());
                break;
            } catch (RemoteExecuteException|IOException e) {
                log.error(String.format("第%d次尝试发布命令失败", counter), e);
                deployEndTime = System.currentTimeMillis();
                rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                            DeploymentStatusBO.CODE_ERROR, 
                                            DeployService.EXEC_ERROR, 
                                            deploymentBO.getEnvironmentName(), 
                                            deploymentBO.getIp(), 
                                            e.getMessage());
                // 不成功等待一会重试
                try {
                    Thread.sleep(TimeConsts.WAIT_TIME);
                } catch (InterruptedException ie) {
                    log.error(ie.getMessage(), ie);
                }
            }
        }
        // 如果需要更新环境
        if (updateEnvironment) {
            for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
                try {
                    Medis.InsertEnvironment(environmentConfig);
                    break;
                } catch (IOException e) {
                    log.error("环境信息更新失败", e);
                }
            }
        }
        dockerService.removeExpireFile(environmentConfig, ConfigConsts.YAML_FILE_TARGET_PATH, "*.yml", TimeConsts.YAML_TIME_DAY);
        rlt.setDeployStartTime(deployStartTime);
        rlt.setDeployEndTime(deployEndTime);
        return rlt;
    }

    /**
     * 用于检查部署结果
     * @author 满显凡
     * @param deploymentBO
     * @return
     */
    public DeploymentStatusBO checkRlt(DeploymentBO deploymentBO) {
        Long processStartTime = System.currentTimeMillis();
        DeploymentStatusBO rlt = null;
        EnvironmentConfig environmentConfig = Medis.getEnvironmentConfigByName(deploymentBO.getEnvironmentName());
        // 检查服务启动情况 
        log.info("开始检测服务启动情况");
        for (int counter = 0; counter < DeployService.DEPLOYMENT_TIME; ++counter) {
            rlt = null;
            try {
                DeploymentCheckBO deploymentCheckBO = this.getDeployRlt(environmentConfig, deploymentBO);
                // 不能确定结果 命令返回值存在失败 则确定为失败  
                if (deploymentCheckBO.getRlt().equals(DeploymentCheckBO.RLT_ERR)) {
                    log.info("不能确定结果 命令返回值为失败 确定为失败");
                    rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                                DeploymentStatusBO.CODE_UNCERTAIN, 
                                                DeployService.CAN_NOT_DETERMINE, 
                                                deploymentBO.getEnvironmentName(), 
                                                deploymentBO.getIp(), 
                                                deploymentCheckBO.getInfo());
                    continue;
                // 有无实例服务 不缺定是否失败 返回结果
                } else if (deploymentCheckBO.getRlt().equals(DeploymentCheckBO.RLT_UNKNOW)) { 
                    log.info("有无实例服务 不能确定是否失败");
                    rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                                DeploymentStatusBO.CODE_UNCERTAIN, 
                                                DeployService.UNCERTAIN_SERVICE, 
                                                deploymentBO.getEnvironmentName(), 
                                                deploymentBO.getIp(), 
                                                deploymentCheckBO.getInfo());
                    continue;
                // 缺少服务 认定为失败 返回现有服务
                } else if (deploymentCheckBO.getRlt().equals(DeploymentCheckBO.RLT_DEFECT)) {
                    log.info("缺少服务 认定为失败");
                    rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                                DeploymentStatusBO.CODE_ERROR, 
                                                DeployService.LACK_SERVICE,
                                                deploymentBO.getEnvironmentName(), 
                                                deploymentBO.getIp(), 
                                                deploymentCheckBO.getInfo());
                    continue;
                // 正常
                } else {
                    log.info("服务部署成功");
                    rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                                DeploymentStatusBO.CODE_OK, 
                                                DeployService.DEPLOYMENT_RLT_OK,
                                                deploymentBO.getEnvironmentName(), 
                                                deploymentBO.getIp(), 
                                                deploymentCheckBO.getInfo());
                    break;
                }
            } catch (RemoteExecuteException|IOException e) {
                log.error(String.format("第%d次尝试检查部署结果失败", counter), e);
                rlt = new DeploymentStatusBO(deploymentBO.getID(), 
                                            DeploymentStatusBO.CODE_ERROR, 
                                            DeployService.CAN_NOT_DETERMINE, 
                                            deploymentBO.getEnvironmentName(), 
                                            deploymentBO.getIp(), 
                                            e.getMessage());
                // 不成功等待一会重试
                try {
                    Thread.sleep(TimeConsts.WAIT_TIME);
                } catch (InterruptedException ie) {
                    log.error(ie.getMessage(), ie);
                }
            }
        }
        rlt.setProcessStartTime(processStartTime);
        rlt.setProcessEndTime(System.currentTimeMillis());
        return rlt;
    }

    /**
     * @author 满显凡
     * @param deploymentBO
     * @return
     * @throws IOException
     * @throws RemoteExecuteException
     */
    private DeploymentCheckBO getDeployRlt(EnvironmentConfig environmentConfig, DeploymentBO deploymentBO) throws RemoteExecuteException, IOException {
        CommandRltBO commandRltBO;
        // 获取检查结果 
        commandRltBO = dockerService.listServices(environmentConfig, DockerConsts.STACK_NAME);
        // 获取失败 
        if (!commandRltBO.isSuccess()) {
            return new DeploymentCheckBO(DeploymentCheckBO.RLT_ERR, commandRltBO.getInfo());
        }

        DeploymentCheckBO deploymentCheckBO = new DeploymentCheckBO();
        
        // 是否有实例少于应有实例数的服务
        boolean uncertain = false;

        // 记录应有服务
        Set<String>  serviceNames = new HashSet<String>();
        for (String image: deploymentBO.getImages()) {
            String[] rImage = image.split("/");
            String[] nameAndTag = rImage[rImage.length - 1].split(":");
            image = nameAndTag[0];
            serviceNames.add(image);
        }

        StringBuffer info = new StringBuffer();
        info.append(commandRltBO.getOk().get(0)).append("\n");

        for (int i = 1; i < commandRltBO.getOk().size(); ++i) {
            String[] rlt = commandRltBO.getOk().get(i).split(" +");
            String serviceName = rlt[1].substring(DockerConsts.STACK_NAME.length() + 1);
            String replicas = rlt[3];
            if (serviceNames.contains(serviceName)) {
                info.append(commandRltBO.getOk().get(i)).append("\n");
                serviceNames.remove(serviceName);
                String[] replicasSplit = replicas.split("/");
                if (Integer.valueOf(replicasSplit[0]) < Integer.valueOf(replicasSplit[1])) {
                    uncertain = true;
                }
            }
        }

        // 服务部署缺失
        if (!serviceNames.isEmpty()) {
            deploymentCheckBO.setRlt(DeploymentCheckBO.RLT_DEFECT);
        // 服务缺失实例
        } else if (uncertain) {
            deploymentCheckBO.setRlt(DeploymentCheckBO.RLT_UNKNOW);
        // 正常情况
        } else {
            deploymentCheckBO.setRlt(DeploymentCheckBO.RLT_OK);
        }
        deploymentCheckBO.setInfo(info.toString());
        return deploymentCheckBO;
    }

}
