package com.lenovodevops.deployment.common.schedule;

import java.io.File;

import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.consts.TimeConsts;
import com.lenovodevops.deployment.common.util.FileUtils;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RemoveYamlFileSchedule {
    
    @Scheduled(fixedRate = 1000L * 60 * 60 * 24)
    public void removeYamlFile() {
        log.info("开始清除本机过期yaml文件");
        // 删除本机yaml文件
        File deleteFile = new File(ConfigConsts.YAML_FILE_PATH);
        File[] deleteFiles = deleteFile.listFiles();
        StringBuffer sb = new StringBuffer();
        for (File file : deleteFiles) {
            Long time = file.lastModified();
            if (file.getName().contains(".yml") && System.currentTimeMillis() - time > 1000L * 60 * 60 * 24 * TimeConsts.YAML_TIME_DAY) {
                sb.append(file.getName()).append(", ");
                FileUtils.deleteFile(file);
            }
        }
        if (sb.length() > 0) {
            sb.replace(sb.length() - 2, sb.length(), "");
            log.info(String.format("删除本机过期yaml文件: %s", sb.toString()));
        }
    }
}
