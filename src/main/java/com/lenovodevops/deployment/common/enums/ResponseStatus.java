package com.lenovodevops.deployment.common.enums;

/**
 * @author 吴迪
 * @date 2021/5/25
 * @explaination 返回状态信息枚举类
 */
public enum ResponseStatus {
    /** 请求成功 */
    SUCCESS(10000, "success"),

    /** 请求失败 */
    FAIL(20000, "fail"),

    /** 文件上传失败 */
    FILE_UPLOAD_FAIL(20001, "fileUploadFail"),

    /** docker集群上执行命令返回错误 */
    COMMAND_FAIL(20002, "commandFail"),

    /** 环境未知 */
    ENVIRONMENT_UNKNOW(20003, "environment unknow"),

    /** 上传命令失败 */
    SEND_COMMAND_FAILD(20004, "send command failed"),

    /** 参数错误 */
    PARAM_ILLEGAL(400, "param illegal"),

    /** 请求体为空 */
    REQUESTBODY_NULL(400, "requestbody null"),

    FILE_IOEXPECTION(500, "file read or write error"),

    SERVICENAME_OVERLAPS(20005, "list servicename overlaps")
    ;
    
    ResponseStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /** 状态码 */
    private int code;

    /** 返回信息 */
    private String msg;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
