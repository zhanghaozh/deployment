package com.lenovodevops.deployment.common.consts;

public class TimeConsts {

    /** 
     * 一次部署记录的过期时间 
     */
    final public static Long TIME_OUT = 1000L * 60 * 60 * 24;

    /**
     * 一次远程操作不成功 等待时间
     */
    final public static Long WAIT_TIME = 1000L * 10;

    /**
     * yaml文件有效期 单位天
     */
    final public static Integer YAML_TIME_DAY = 7;
    
}
