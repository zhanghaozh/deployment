package com.lenovodevops.deployment.service.impl;

import com.lenovodevops.deployment.common.enums.ResponseStatus;
import com.lenovodevops.deployment.model.ao.GetBranchesAO;
import com.lenovodevops.deployment.model.exception.GitFailException;
import com.lenovodevops.deployment.model.exception.OverlapsException;
import com.lenovodevops.deployment.service.BranchService;
import com.lenovodevops.deployment.service.ListService;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class BranchServiceImpl implements BranchService {

    @Autowired
    ListService listService;
    /**
     * @author 吴迪
     * @param gitParam 存储git地址、用户名、密码的字符串（用|分隔）
     * @return git分支列表
     * @throws Exception
     */
    @Override
    public String getRemoteBranches(String gitParam) throws Exception {
        try {
            GetBranchesAO getBranchesAO = this.getAO(gitParam);
            Collection<Ref> credentialList;
            if(StringUtils.isNotEmpty(getBranchesAO.getUsername())&&StringUtils.isNotEmpty(getBranchesAO.getPassword())){
                UsernamePasswordCredentialsProvider provider = new UsernamePasswordCredentialsProvider(getBranchesAO.getUsername(), getBranchesAO.getPassword());
                credentialList = Git.lsRemoteRepository().setRemote(getBranchesAO.getGitAddress()).setCredentialsProvider(provider).call();
            }else {
                credentialList = Git.lsRemoteRepository().setRemote(getBranchesAO.getGitAddress()).call();
            }
            List<String> branchList = new ArrayList<>(10);
            //设置筛选条件（待修改）
            for (Ref ref : credentialList) {
                String refName = ref.getName();
                if (refName.startsWith("refs/heads/")) {                       //需要进行筛选
                    String branchName = refName.replace("refs/heads/", "");
                    branchList.add(branchName);
                }
            }
            StringBuilder stringBuffer = new StringBuilder();
            for (String str : branchList) {
                stringBuffer.append(str).append("|");
            }
            stringBuffer.replace(stringBuffer.length() - 1, stringBuffer.length(),"");
            return stringBuffer.toString();
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw e;
        }
    }


    private GetBranchesAO getAO(String gitParam) throws IOException, GitFailException, OverlapsException {
        String[] tempStr= gitParam.split("\\|");
        if(tempStr.length != 3){
            throw new GitFailException(ResponseStatus.PARAM_ILLEGAL);
        }
        String gitaddress = this.getGitAddress(tempStr[0]);
        if(gitaddress == null){
            throw new GitFailException("git地址下未找到此item,请检查清单");
        }
        GetBranchesAO getBranchesAO = new GetBranchesAO();
        getBranchesAO.setGitAddress(gitaddress);
        getBranchesAO.setUsername(tempStr[1]);
        getBranchesAO.setPassword(tempStr[2]);
        return getBranchesAO;
    }

    private String getGitAddress(String itemName) throws IOException, OverlapsException {
        String listContent = listService.readLocalCheckList();
        String[] tempContent = listContent.split(",");
        for (String tempItem: tempContent) {
            if (tempItem == null){
                break;
            }
            String[] tempItemName = tempItem.split("\\|");
            if(tempItemName[0].equals(itemName)){
                return tempItemName[1];
            }
        }
        return null;
    }
}
