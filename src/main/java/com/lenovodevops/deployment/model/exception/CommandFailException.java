package com.lenovodevops.deployment.model.exception;

import com.lenovodevops.deployment.common.enums.ResponseStatus;

/**
 * 命令在环境中执行返回失败则抛出
 * @author 满显凡
 */
public class CommandFailException extends SystemException {

    public CommandFailException(ResponseStatus responseStatus) {
        super(responseStatus.getCode(), responseStatus.getMsg());
    }

    public CommandFailException(ResponseStatus responseStatus, String extMsg) {
        super(responseStatus.getCode(), responseStatus.getMsg() + extMsg);
    }

    public CommandFailException(String msg) {
        super(msg);
    }

}
