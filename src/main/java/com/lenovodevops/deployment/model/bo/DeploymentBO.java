package com.lenovodevops.deployment.model.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * 一次部署请求需要的环境，镜像在这里，可以添加指定的stackname
 * @author 满显凡
 */
public class DeploymentBO {
    
    // 一次部署请求的发起时间
    private Long createTime;
    // 本次请求目标环境名 
    private String environmentName;
    // 本次请求的目标ip 
    private String ip;
    // 本次请求需要部署的镜像 
    private List<String> images;

    public DeploymentBO() {
        images = new ArrayList<String>();
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public List<String> getImages() {
        return images;
    }

    public void addImages(String image) {
        this.images.add(image);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEnvironmentName() {
        return environmentName;
    }

    public void setEnvironmentName(String environmentName) {
        this.environmentName = environmentName;
    }

    public String getID() {
        return String.format("%s-%s", this.environmentName, Long.toString(this.createTime));
    }

    /**
     * 获取本次部署的yml名字
     * @return
     */
    public String getYamlName() {
        return String.format("%s-%s.yml", this.environmentName, Long.toString(this.createTime));
    }


}
