package com.lenovodevops.deployment.model.exception;

import com.lenovodevops.deployment.common.enums.ResponseStatus;


/**
 * @author 吴迪
 * @date 2021/6/11
 * @explaination 清单服务名重复异常
 */
public class OverlapsException extends SystemException{
    public OverlapsException(ResponseStatus status) {
        super(status.getCode(), status.getMsg());
    }

    public OverlapsException(String msg) {
        super(msg);
    }
}
