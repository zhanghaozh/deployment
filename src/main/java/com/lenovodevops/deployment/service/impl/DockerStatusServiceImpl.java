package com.lenovodevops.deployment.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lenovodevops.deployment.common.consts.DockerConsts;
import com.lenovodevops.deployment.common.enums.ResponseStatus;
import com.lenovodevops.deployment.db.EnvironmentConfig;
import com.lenovodevops.deployment.db.Medis;
import com.lenovodevops.deployment.model.ao.RestartServiceAO;
import com.lenovodevops.deployment.model.ao.RmServiceListAO;
import com.lenovodevops.deployment.model.bo.CommandRltBO;
import com.lenovodevops.deployment.model.bo.ServiceStatusBO;
import com.lenovodevops.deployment.model.exception.CommandFailException;
import com.lenovodevops.deployment.model.exception.EnvironmentException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;
import com.lenovodevops.deployment.model.vo.ListEnvironmentsVO;
import com.lenovodevops.deployment.model.vo.ListServicesByEnvironmentsVO;
import com.lenovodevops.deployment.model.vo.RestartServiceVO;
import com.lenovodevops.deployment.model.vo.RmServiceListVO;
import com.lenovodevops.deployment.service.DockerService;
import com.lenovodevops.deployment.service.DockerStatusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("DockerStatusSericeImpl")
public class DockerStatusServiceImpl implements DockerStatusService {

    @Autowired
    @Qualifier("DockerServiceImpl")
    private DockerService dockerService;

    /**
     * 返回所有环境名
     * @return
     */
    @Override
    public ListEnvironmentsVO listEnvironments() {
        List<String> environmentNames = Medis.getAllEnvironmentsName();
        return new ListEnvironmentsVO(environmentNames.toArray(new String[environmentNames.size()]));
    }

    /**
     * 返回指定环境下的service名字和状态
     *
     * @author 满显凡
     * @throws EnvironmentException
     */
    @Override
    public ListServicesByEnvironmentsVO listServicesByEnvironments(String environmentName) throws CommandFailException, RemoteExecuteException, IOException, EnvironmentException {
        EnvironmentConfig environmentConfig = Medis.getEnvironmentConfigByName(environmentName);
        if (environmentConfig == null) {
            throw new EnvironmentException(ResponseStatus.ENVIRONMENT_UNKNOW);
        }
        try {
            List<ServiceStatusBO> services = new ArrayList<ServiceStatusBO>();
            CommandRltBO commandRltBO = dockerService.listServices(environmentConfig, DockerConsts.STACK_NAME);
            if (!commandRltBO.isSuccess()) {
                throw new CommandFailException(ResponseStatus.COMMAND_FAIL, commandRltBO.getErrString());
            }
            for (int i = 1; i < commandRltBO.getOk().size(); ++i) {
                String[] rlt = commandRltBO.getOk().get(i).split(" +");
                ServiceStatusBO serviceStatusBO = new ServiceStatusBO();
                serviceStatusBO.setService(rlt[1].substring(DockerConsts.STACK_NAME.length() + 1));
                serviceStatusBO.setReplicas(rlt[3]);
                services.add(serviceStatusBO);
            }
            return new ListServicesByEnvironmentsVO(services);
        } catch(RemoteExecuteException|IOException e) {
            log.error(e.getMessage(), e);
            throw e;
        }

    }

    /**
     * 重启指定服务
     * @author 满显凡
     * @param restartServiceRQB
     * @return
     * @throws CommandFailException
     * @throws EnvironmentException
     */
    @Override
    public RestartServiceVO restartService(RestartServiceAO restartServiceRQB) throws CommandFailException, RemoteExecuteException, IOException, EnvironmentException {
        String environmentName = restartServiceRQB.getEnvironment();
        String serviceName = restartServiceRQB.getService();
        EnvironmentConfig en = Medis.getEnvironmentConfigByName(environmentName);
        if (en == null) {
            throw new EnvironmentException(ResponseStatus.ENVIRONMENT_UNKNOW);
        }
        try {
            CommandRltBO rlt = dockerService.restartService(en, DockerConsts.STACK_NAME + "_" + serviceName);
            if (!rlt.isSuccess()) {
                throw new CommandFailException(ResponseStatus.COMMAND_FAIL, rlt.getErrString());
            }
            return new RestartServiceVO(serviceName);
        } catch(RemoteExecuteException|IOException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * @author 满显凡
     * @throws IOException
     * @throws RemoteExecuteException
     * @throws EnvironmentException
     */
    @Override
    public RmServiceListVO rmServiceList(RmServiceListAO rmServiceListAO) throws RemoteExecuteException, IOException, EnvironmentException, CommandFailException {
        EnvironmentConfig en = Medis.getEnvironmentConfigByName(rmServiceListAO.getEnvironment());
        if (en == null) {
            throw new EnvironmentException(ResponseStatus.ENVIRONMENT_UNKNOW);
        }
        CommandRltBO rlt = dockerService.rmServiceList(en, DockerConsts.STACK_NAME, rmServiceListAO.getServices());
        if (!rlt.isSuccess()) {
            throw new CommandFailException(ResponseStatus.COMMAND_FAIL, rlt.getErrString());
        }
        return new RmServiceListVO(rmServiceListAO.getServices());
    }


    /**
     * @author 吴迪
     * @date 2021/6/7
     * @explaination 移除环境并返回服务名列表
     * */
    @Override
    public RmServiceListVO rmServicesInStack(String environment) throws IOException, CommandFailException, EnvironmentException, RemoteExecuteException{
        EnvironmentConfig en = Medis.getEnvironmentConfigByName(environment);
        if(en == null){
            //若环境信息为空，则抛出环境未知异常
            throw new EnvironmentException(ResponseStatus.ENVIRONMENT_UNKNOW);
        }
        try {
            CommandRltBO result = dockerService.listServices(en, DockerConsts.STACK_NAME);
            List<String> set = new ArrayList<>();
            if(!result.isSuccess()){
                throw new CommandFailException(ResponseStatus.COMMAND_FAIL, result.getErrString());
            }
            for (int i = 1; i < result.getOk().size(); ++i) {
                String[] rlt = result.getOk().get(i).split(" +");
                String str = rlt[1].substring(DockerConsts.STACK_NAME.length() + 1);
                set.add(str);
            }
            String[] services = set.toArray(new String[0]);
            CommandRltBO rlt = dockerService.rmStack(en, DockerConsts.STACK_NAME);
            if (!rlt.isSuccess()) {
                throw new CommandFailException(ResponseStatus.COMMAND_FAIL, rlt.getErrString());
            }
            return new RmServiceListVO(services);
        }catch (IOException e){
            log.error(e.getMessage(), e);
            throw e;
        }

    }
}
