package com.lenovodevops.deployment.common.util;

public class DockerCommandUtils {
    
    /**
     * docker stack deploy $(yaml文件名称) $(stack名称)
     * @author 满显凡
     * @param pathAndName yaml文件在集群中的全名（路径+文件名）
     * @param stackName
     * @return
     */
    public static String deployStack(String pathAndName, String stackName) {
        return String.format("docker stack deploy -c %s %s", pathAndName, stackName);
    }

    /**
     * docker stack rm $(stack名称)
     * @author 满显凡
     * @param stackName
     * @return
     */
    public static String rmStack(String stackName) {
        return String.format("docker stack rm %s", stackName);
    }

    /**
     * docker stack services $(stack_name)
     * 查询指定stack下的服务
     * @author 满显凡
     * @param stackName
     * @return
     */
    public static String dockerStackServices(String stackName) {
        return String.format("docker stack services %s", stackName);
    }

    /**
     * docker service update $(service_name) --force
     * @author 满显凡
     * @param serviceName
     * @return
     */
    public static String restartService(String serviceName) {
        return String.format("docker service update %s --force", serviceName);
    }

    /**
     * docker service rm stackname_nginx
     * @author 满显凡
     * @param stackName 在docker集群中服务名为（堆栈名_服务名）此参数为堆栈名
     * @param serviceName 此参数为服务名
     * @return
     */
    public static String rmService(String stackName, String serviceName) {
        return String.format("docker service rm %s_%s", stackName, serviceName);
    }

    /**
     * docker service rm stackname_nginx stackname_sql
     * @author 吴迪
     * @paramm stackName 堆栈名
     * @paramm serviceNameList 此参数为服务名列表
     */
    public static String rmServiceList(String stackName, String[] serviceNameList) {
        StringBuffer paramString = new StringBuffer();
        for (String serviceName: serviceNameList) {
            paramString.append(stackName).append("_").append(serviceName).append(" ");
        }
        return String.format("docker service rm %s", paramString);
    }

}
