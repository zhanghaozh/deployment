package com.lenovodevops.deployment.db;

/**
 * @author 吴迪
 * @date 2021/5/24
 * @explaination 自定义缓存的实体类
 */

public class Cache {

    /** 缓存key */
    private String key;
    /** 缓存value */
    private Object value;
    /** 缓存过期时间 */
    private Long timeout;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Long getTimeout() {
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

}
