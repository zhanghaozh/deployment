package com.lenovodevops.deployment.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.util.YamlUtils;

/**
 * 读取，存储自定义yaml配置
 */
public class YamlMemory {

    /**
     * 自定义yaml文件被修改的时间
     */
    private static Long lastModified;

    private static Map<Object, Object> customYaml;

    /**
     * @author 满显凡
     * 初始化yaml文件
     */
    static {
        try {
            File file = new File(ConfigConsts.CUSTOM_YAML_PATH_NAME);
            lastModified = file.lastModified();
            customYaml = YamlUtils.loadYaml(ConfigConsts.CUSTOM_YAML_PATH_NAME);
        } catch (FileNotFoundException e) {
            customYaml = new LinkedHashMap<>();
            e.printStackTrace();
        }
    }

    /**
     * 检查更新并返回最新版yaml
     * @author 满显凡
     * @return
     */
    public static Map<Object, Object> getCustomYaml() {
        try {
            File file = new File(ConfigConsts.CUSTOM_YAML_PATH_NAME);
            if (!file.exists()) {
                throw new FileNotFoundException(ConfigConsts.CUSTOM_YAML_PATH_NAME);
            }
            if (file.lastModified() > lastModified) {
                customYaml = YamlUtils.loadYaml(ConfigConsts.CUSTOM_YAML_PATH_NAME);
                lastModified = file.lastModified();
            }
            return customYaml;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return customYaml;
        } 
    }
    
}
