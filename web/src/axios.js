import axios from 'axios'

axios.defaults.baseURL = 'http://39.96.43.76:8081'
axios.defaults.withCredentials = true
axios.interceptors.request.use(config => {
  return config
})

axios.interceptors.response.use(response => {
  return response
})
