package com.lenovodevops.deployment.model.bo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceStatusBO {
    
    private String service;
    private String replicas;
 
}
