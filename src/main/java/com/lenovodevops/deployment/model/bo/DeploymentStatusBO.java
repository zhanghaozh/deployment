package com.lenovodevops.deployment.model.bo;

import com.lenovodevops.deployment.common.util.DateUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 满显凡
 */
@Getter
@Setter
@NoArgsConstructor
public class DeploymentStatusBO {
    
    /** ############ 第一次部署产生的结果 ############ */
    /** 部署code  部署中*/
    final public static Integer CODE_DEPLOYING = 1;
    /** 部署code  部署完毕*/
    final public static Integer CODE_DONE = 2;
    //final public static Integer CODE_ERROR = 5; 在后面有 这个状态在两种操作下都可能产生

    /** ############ 部署完成后第一次获取结果产生的结果 ############ */
    /** 部署code  待检查 */
    final public static Integer CODE_CHECK = 3;
    
    /** ############ 部署完成后第二+次检查后产生的结果 ############ */
    /** 部署code  部署成功*/
    final public static Integer CODE_OK = 4;
    /** 部署code  部署网络错误*/
    final public static Integer CODE_ERROR = 5;
    /** 部署code  结果不确定 需开发人员确认 */
    final public static Integer CODE_UNCERTAIN = 6;

    /** 此次部署的id */ 
    private String deploymentID;
    /** 状态码 */
    private Integer code;
    /** 状态信息 */ 
    private String msg;
    /** 环境名 */ 
    private String environmentName;
    /** IP */
    private String ip;
    /** 处理的开始时间 */ 
    private Long processStartTime;
    /** 处理结束时间 */ 
    private Long processEndTime;
    /** 部署开始时间 */ 
    private Long deployStartTime;
    /** 部署结束时间 */ 
    private Long deployEndTime;
    /** terminal输出 或 错误信息 */ 
    private String info;
    
    public DeploymentStatusBO(String deploymentID, Integer code, String msg) {
        this.deploymentID = deploymentID;
        this.code = code;
        this.msg = msg;
    }

    public DeploymentStatusBO(String deploymentID, Integer code, String msg, String environmentName, String ip, String info) {
        this.deploymentID = deploymentID;
        this.code = code;
        this.msg = msg;
        this.environmentName = environmentName;
        this.ip = ip;
        this.info = info;
        this.processStartTime = 0L;
        this.processEndTime = 0L;
        this.deployStartTime = 0L;
        this.deployEndTime = 0L;
    }
    
    @Override
    public String toString() {
        if (CODE_DEPLOYING.equals(this.code)) {
            return String.format("rlt: %s\nid: %s", this.msg, this.deploymentID);
        } else if (CODE_ERROR.equals(this.code) || CODE_OK.equals(this.code) || CODE_UNCERTAIN.equals(this.code)) {
            return String.format("rlt: %s\nid: %s\nprocess_time: %s ms\ndeploy_time: %s ms\n%s", 
                this.msg, 
                this.deploymentID,
                Long.toString(this.processEndTime - this.processStartTime),
                Long.toString(this.deployEndTime - this.deployStartTime),
                this.info);
        } else if (CODE_DONE.equals(this.code)) {
            return String.format("rlt: %s\nid: %s\nprocess_time: %s ms\ndeploy_time: %s ms\n%s", 
                this.msg, 
                this.deploymentID,
                Long.toString(this.processEndTime - this.processStartTime),
                Long.toString(this.deployEndTime - this.deployStartTime),
                "可发送相同请求查看部署结果\n" + this.info);
        } else {
            return null;
        }
    }

    public String toLogString() {
        StringBuffer sb = new StringBuffer();
        sb.append("{ id: ")
            .append(this.deploymentID).append(", ")
            .append("code: ")
            .append(Integer.toString(this.code)).append(", ")
            .append("msg: ")
            .append(this.msg).append(", ")
            .append("environment: ")
            .append(this.environmentName).append(", ")
            .append("ip: ")
            .append(this.ip).append(", ")
            .append("start_time: ")
            .append(DateUtils.getTime(this.processStartTime)).append(", ")
            .append("process_time: ").append(" ms")
            .append(Long.toString(this.processEndTime - this.processStartTime)).append(", ")
            .append("deploy_time：")
            .append(Long.toString(this.deployEndTime - this.deployStartTime));
        if (this.info != null ) {
            sb.append(" ms, \n")
                .append("info: { \n")
                .append(this.info)
                .append("\n} }\n");
        } else {
            sb.append(" ms } \n");
        }
           
        return sb.toString();
    }

}
