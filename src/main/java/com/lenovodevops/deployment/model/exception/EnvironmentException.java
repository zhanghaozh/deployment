package com.lenovodevops.deployment.model.exception;

import com.lenovodevops.deployment.common.enums.ResponseStatus;

public class EnvironmentException extends SystemException {
    
    public EnvironmentException(ResponseStatus responseStatus) {
        super(responseStatus.getCode(), responseStatus.getMsg());
    }

    public EnvironmentException(String msg) {
        super(msg);
    }
}