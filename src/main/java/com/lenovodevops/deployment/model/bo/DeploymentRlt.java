package com.lenovodevops.deployment.model.bo;

import lombok.Getter;
import lombok.Setter;

import com.lenovodevops.deployment.service.DeployService;

/**
 * 用于记录一次部署的结果
 * @author 满显凡
 */
@Getter
@Setter
public class DeploymentRlt {
    
    private String rlt;
    private String deploymentID;
    private String environmentName;
    private String ok;
    private String err;
    private String services;
    private Long processStartTime;
    private Long processEndTime;
    private Long deployStartTime;
    private Long deployEndTime;

    public DeploymentRlt(String rlt, String deploymentID, String environmentName) {
        this.rlt = rlt;
        this.deploymentID = deploymentID;
        this.environmentName = environmentName;
        this.ok = "";
        this.err = "";
        this.services = "";
        this.deployStartTime = 0L;
        this.deployEndTime = 0L;
    }

    public DeploymentRlt(String deploymentID, String environmentName, 
        CommandRltBO commandRlt) {
        this.rlt = DeployService.DEPLOYMENT_RLT_OK;
        this.deploymentID = deploymentID;
        this.environmentName = environmentName;
        this.ok = commandRlt.getOkString();
        this.err = commandRlt.getErrString();
        this.services = "";
    }

    public DeploymentRlt(String rlt, String deploymentID, 
        String environmentName, CommandRltBO commandRlt) {
        this.rlt = rlt;
        this.deploymentID = deploymentID;
        this.environmentName = environmentName;
        this.ok = commandRlt.getOkString();
        this.err = commandRlt.getErrString();
        this.services = "";
    }

    /**
     * 写在消息队列里的信息
     * @author 满显凡
     * @return
     */
    public String toSimpleString() {
        StringBuffer rlt = new StringBuffer(String.format("%s|%s|%s|%s|%s",
            this.rlt,
            this.deploymentID, 
            this.environmentName, 
            Long.toString(this.processEndTime - this.processStartTime),
            Long.toString(this.deployEndTime - this.deployStartTime)));
        if (this.err != null && this.err.length() > 0) {
            rlt.append("|").append(this.err);
        }
        if (this.services != null && this.services.length() > 0) {
            rlt.append("|").append(this.services);
        }
        return rlt.toString();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.rlt)
            .append(", ")
            .append(this.deploymentID)
            .append(", ")
            .append(this.environmentName)
            .append(", ")
            .append(Long.toString(this.processEndTime - this.processStartTime))
            .append(", ")
            .append(Long.toString(this.deployEndTime - this.deployStartTime))
            .append(", ok[ ")
            .append(this.ok)
            .append(" ]")
            .append(", error[ ")
            .append(this.err)
            .append(" ]")
            .append(", abnormalService[ ")
            .append(this.services)
            .append(" ]");
        return sb.toString();
    }

}
