import Vue from 'vue'
import VueRouter from 'vue-router'
import Deploy from '../views/Deploy.vue'
import DeployService from '../views/DeployService'
import DeleteService from '../views/DeleteService'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    redirect: {name: 'Deploy'}
  },
  {
    path: '/deploy',
    name: 'Deploy',
    component: Deploy
  },
  {
    path: '/service',
    name: 'Service',
    component: DeployService
  },
  {
    path: '/delete_service',
    name: 'Delete',
    component: DeleteService

  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
