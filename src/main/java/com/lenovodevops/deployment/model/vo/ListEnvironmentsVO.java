package com.lenovodevops.deployment.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 返回全部环境名
 * @author 满显凡
 */
@Getter
@Setter
@AllArgsConstructor
public class ListEnvironmentsVO {
    
    private String[] environments;
}
