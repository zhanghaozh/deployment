package com.lenovodevops.deployment.service.impl;

import com.lenovodevops.deployment.common.schedule.ListContentInit;
import com.lenovodevops.deployment.common.util.CacheManagerUtils;
import com.lenovodevops.deployment.service.ListService;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 吴迪
 * @date 2021/5/24
 * @explaination 读取缓存中清单内容
 * @return 清单的内容(缓存)
 * @throws IOException,OverlapsException
 */
@Service
@Slf4j
public class ListServiceImpl implements ListService {

    CacheManagerUtils cacheManagerUtils =  CacheManagerUtils.getInstance();

    @Override
    public String readLocalCheckList(){
        Object rlt = cacheManagerUtils.get(ListContentInit.KEY_ID);
        if(rlt == null){
            log.error("The cache missed");
        }else {
            return rlt.toString();
        }
        return null;
    }

}
