package com.lenovodevops.deployment.model.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LogEntity {

    private String type;
    private String url;
    private String ip;
    private String method;
    private String param;
    private String result;
    
}
