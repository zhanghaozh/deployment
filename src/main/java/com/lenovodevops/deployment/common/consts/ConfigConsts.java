package com.lenovodevops.deployment.common.consts;

public class ConfigConsts {
    
    /** 
     * yaml文件在本系统的存储路径 
     */
    final public static String YAML_FILE_PATH = "/home/yamlStorage/";

    /** 
     * 部署日志路径 
     */
    final public static String LOG_PATH_NAME = "/home/logStorage/deployment.log";

    /**
     * 清单文件的路径
     */
    public static String LIST_FILE_PATH = "/home/listStorage/list.txt";

    /** 
     * environments 持久化文件位置 
     */
    final public static String ENVIRONMENTS_CONFIG_PATH_NAME = "/home/longStorage/environments.config";

    /**
     * 自定义yaml文件的位置
     */
    final public static String CUSTOM_YAML_PATH_NAME = "/home/longStorage/services.yml";

    /** 
     * yaml文件在docker集群的存储路径 
     */
    final public static String YAML_FILE_TARGET_PATH = "/home/yaml/";

    /** 本机的ssh密钥 */
    final public static String AUTHORITY_KEY_PATH = "/root/.ssh/id_rsa";

//---------------------------------------------------------------------------------------
    // /** 
    //  * yaml文件在本系统的存储路径 
    //  */
    // final public static String YAML_FILE_PATH = "D:/Course/实训/file/";

    // /** 
    //  * 部署日志路径 
    //  */
    // final public static String LOG_PATH_NAME = "D:/Course/实训/file/deployment.log";

    // /**
    //  * 清单文件的路径
    //  */
    // public static String LIST_FILE_PATH = "./list.txt";

    // /** 
    //  * environments 持久化文件位置 
    //  */
    // final public static String ENVIRONMENTS_CONFIG_PATH_NAME = "D:/Course/实训/file/environments.config";

    // /**
    //  * 自定义yaml文件的位置
    //  */
    // final public static String CUSTOM_YAML_PATH_NAME = "./services.yml";

    // /** 
    //  * yaml文件在docker集群的存储路径 
    //  */
    // final public static String YAML_FILE_TARGET_PATH = "/home/yaml/";

    // /** 本机的ssh密钥 */
    // final public static String AUTHORITY_KEY_PATH = "./id_rsa";
    
}
