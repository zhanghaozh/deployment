package com.lenovodevops.deployment.model.exception;

import com.lenovodevops.deployment.common.enums.ResponseStatus;

/**
 * 读取environment本地文件时，如果文件不完整则抛出该异常
 * @author 满显凡
 */
public class ConfigFileException extends SystemException {
    
    public ConfigFileException(ResponseStatus responseStatus) {
        super(responseStatus.getCode(), responseStatus.getMsg());
    }

    public ConfigFileException(String msg) {
        super(msg);
    }
}
