package com.lenovodevops.deployment.common.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.lenovodevops.deployment.db.Cache;

import lombok.extern.slf4j.Slf4j;


/**
 * @author 吴迪
 * @date 2021/5/24
 * @explaination 自定义缓存工具类
 */
@Slf4j
public class CacheManagerUtils {

    private Map<String, Cache> cacheMap = new ConcurrentHashMap<String, Cache>();

    private static CacheManagerUtils cacheManagerUtils = new CacheManagerUtils();

    private CacheManagerUtils() {
    }

    public void put(String key, Object value){
        put(key,value,null);
    }

    public static CacheManagerUtils getInstance() {
        return CacheManagerUtils.cacheManagerUtils;
    }

    /**
    * @author 吴迪
    * @date 2021/5/24
    * @explaination 添加缓存（设置缓存有效时间）
    */
    public void put(String key, Object value, Long timeout){
        Cache cache = new Cache();
        cache.setKey(key);
        cache.setValue(value);
        if (timeout != null){
            cache.setTimeout(System.currentTimeMillis() + timeout);
        }
        cacheMap.put(key,cache);
    }

    /**
     * @author 吴迪
     * @date 2021/5/24
     * @explaination 使用key来查询缓存
     */
    public Object get(String key){
        Cache cache = cacheMap.get(key);
        if(cache == null){
            return null;
        }
        if(cache.getTimeout() == null){
            return cache.getValue();
        }
        Long temptime = System.currentTimeMillis();
        long timeInterval = temptime - cache.getTimeout();
        //当timeInterval（时间间隔）>= 0时，代表当前key已过期，但是没被定期删除（未到检查时间），故此时get应返回null值。
        if (timeInterval < 0 ){
            return cache.getValue();
        }
        return null;
    }

    /**
     * @author 吴迪
     * @date 2021/5/24
     * @explaination 使用key来删除缓存
     */
    public synchronized void remove(String key){
        cacheMap.remove(key);
    }

    /**
     * @author 吴迪
     * @date 2021/5/24
     * @explaination 定时检查有效期并清除过期缓存
     */
    synchronized public void checkAndRemove(){
        log.info("开始清除过期缓存");
        StringBuffer sb = new StringBuffer();
        for (String key : cacheMap.keySet()){
            Cache cache = cacheMap.get(key);
            if (cache == null){
                break;
            }
            Long timeout = ((Cache)cache).getTimeout();
            if (timeout == null) {
                continue;
            }
            Long temptime = System.currentTimeMillis();
            if (temptime - timeout > 0){
                sb.append(key).append(", ");
                cacheMap.remove(key);
            }
        }
        if (sb.length() > 0) {
            sb.replace(sb.length() - 2, sb.length(), "");
            log.info(String.format("清理缓存，删除的key： %s", sb.toString()));
        }
    }
}