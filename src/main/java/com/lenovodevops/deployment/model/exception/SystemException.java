package com.lenovodevops.deployment.model.exception;

public class SystemException extends Exception {
    
    private int code;
    
    public SystemException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    public SystemException(String msg) {
        super(msg);
        this.code = -1;
    }

    public int getCode() {
        return code;
    }

}
