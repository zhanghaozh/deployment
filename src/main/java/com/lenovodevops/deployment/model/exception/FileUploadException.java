package com.lenovodevops.deployment.model.exception;

import com.lenovodevops.deployment.common.enums.ResponseStatus;

/**
 * 向指定主机上传文件失败时抛出的异常
 * @author 满显凡
 */
public class FileUploadException extends SystemException {
    
    public FileUploadException(ResponseStatus responseStatus, String msg) {
        super(responseStatus.getCode(), responseStatus.getMsg() + "," + msg);
    }

    public FileUploadException(String msg) {
        super(msg);
    }
  
}
