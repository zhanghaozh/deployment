package com.lenovodevops.deployment.model.ao;

import lombok.Getter;
import lombok.Setter;


/**
 * @author 吴迪
 * @date 2021/5/31
 * @explaination 存储Git地址用户名和密码的数据对象
 */
@Getter
@Setter
public class GetBranchesAO {

    private String gitAddress;

    private String username;

    private String password;
}
