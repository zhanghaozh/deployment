package com.lenovodevops.deployment.model.exception;

/**
 * @author 满显凡
 */
public class ParamIllegalException extends Exception {

    public ParamIllegalException(String msg) {
        super(msg);
    }
    
}
