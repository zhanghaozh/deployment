package com.lenovodevops.deployment.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RmServiceListVO {
    
    private String[] services;
    
}
