package com.lenovodevops.deployment.controller;

import com.lenovodevops.deployment.annotation.Log;
import com.lenovodevops.deployment.model.exception.GitFailException;
import com.lenovodevops.deployment.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 吴迪
 * @date 2021/5/31
 * @explaination 获取git地址下的所有分支
 */
@RestController
public class GitBranchController {

    @Autowired
    BranchService branchService;

    @Log
    @PostMapping("/branches")
    public String getBranchList(@RequestBody String gitParam) throws Exception {
       return branchService.getRemoteBranches(gitParam);
    }
}
