package com.lenovodevops.deployment.controller;

import java.io.IOException;

import javax.validation.Valid;

import com.lenovodevops.deployment.annotation.Log;
import com.lenovodevops.deployment.model.ao.RmServiceListAO;
import com.lenovodevops.deployment.model.exception.CommandFailException;
import com.lenovodevops.deployment.model.exception.EnvironmentException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;
import com.lenovodevops.deployment.model.vo.RmServiceListVO;
import com.lenovodevops.deployment.service.DockerStatusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 吴迪
 * @date 2021/5/25
 * @explaination 服务回收的接口设计
 * */
@RestController
public class CollectController {

    @Autowired
    @Qualifier("DockerStatusSericeImpl")
    private DockerStatusService dockerStatusService;


    /**
     * @author 吴迪
     * @throws EnvironmentException
     * @date 2021/5/25
     * @explaination 删除指定的服务名
     * @paramm 环境名（需要提供）
     * */
    @Log
    @PostMapping("/service/delete_services")
    public RmServiceListVO deleteServices(@Valid @RequestBody RmServiceListAO rmServiceListAO) throws RemoteExecuteException, IOException, EnvironmentException, CommandFailException {
        return dockerStatusService.rmServiceList(rmServiceListAO);
    }


    /**
     * @author 吴迪
     * @date 2021/6/7
     * @explaination 移除指定环境下的所有服务
     * @return 移除的所有服务名
     */
    @Log
    @PostMapping("/services/delete_environment")
    public RmServiceListVO deleteServicesInStack(@RequestParam String environmentName) throws  IOException, CommandFailException, EnvironmentException, RemoteExecuteException {
            return dockerStatusService.rmServicesInStack(environmentName);
    }

}
