package com.lenovodevops.deployment.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 返回重启的服务名
 * @author 满显凡
 */
@Getter
@Setter
@AllArgsConstructor
public class RestartServiceVO {
    
    private String service;
}
