package com.lenovodevops.deployment.service;

import com.lenovodevops.deployment.model.exception.OverlapsException;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
public interface ListService {

     /**
      * @author 吴迪
      * @date 2021/5/24
      * @explaination 读取缓存中清单内容
      * @return 清单的内容(缓存)
      */
     String readLocalCheckList() throws IOException, OverlapsException;
}
