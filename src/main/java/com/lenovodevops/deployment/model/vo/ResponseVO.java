package com.lenovodevops.deployment.model.vo;

import com.alibaba.fastjson.JSONObject;
import com.lenovodevops.deployment.common.enums.ResponseStatus;


/**
 * @author 吴迪
 * @date 2021/5/25
 * @explaination JsonObject的封装类
 */
public class ResponseVO<T> {

    /** 数据 */
    private T data;

    /** 状态码 */
    private int code;

    /** 返回信息 */
    private String msg;

    public ResponseVO(ResponseStatus status, T data){
        this.code = status.getCode();
        this.msg = status.getMsg();
        this.data = data;
    }

    public ResponseVO(int code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResponseVO(T data) {
        this.data = data;
        this.code = ResponseStatus.SUCCESS.getCode();
        this.msg = ResponseStatus.SUCCESS.getMsg();
    }

    public ResponseVO() {
        this.code = ResponseStatus.SUCCESS.getCode();
        this.msg = ResponseStatus.SUCCESS.getMsg();
        this.data = null;
    }

    public JSONObject toJSONObject(){
        JSONObject obj = new JSONObject();
        obj.put("code", code);
        obj.put("message", msg);
        obj.put("data", data);
        return obj;
    }

    @Override
    public String toString() {
        return toJSONObject().toString();
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    
}
