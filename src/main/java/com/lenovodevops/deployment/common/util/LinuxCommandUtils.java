package com.lenovodevops.deployment.common.util;

/**
 * 用于生成linux命令
 * @author 满显凡
 */
public class LinuxCommandUtils {
    
    /**
     * @author 满显凡
     * @param path
     * @param time 单位天
     * @return
     */
    public static String removeExpireFile(String path, String file, Integer time) {
        return String.format("find %s -name \"%s\" -mtime +%d -exec rm {} \\;", path, file, time);
    }

}
