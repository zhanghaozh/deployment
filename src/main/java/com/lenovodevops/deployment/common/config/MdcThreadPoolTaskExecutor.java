package com.lenovodevops.deployment.common.config;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import com.alibaba.fastjson.JSONObject;
import com.lenovodevops.deployment.common.aspect.LogAspect;

import org.slf4j.MDC;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import lombok.extern.slf4j.Slf4j;

/**
 * 在使用@Sync时，Spring默认提供的TaskExecutor下的ThreadPoolTaskExecutor无法支持MDC在线程切换时上下文内容的继承。因为MDC本质上使用ThreadLocal来保存上下文。
 * 重写ThreadPoolTaskExecutor，在callable(真实处理过程)前注入父线程内容。
 * @author 满显凡
 */
@Slf4j
public class MdcThreadPoolTaskExecutor extends ThreadPoolTaskExecutor {
 
    @Override
    public <T> Future<T> submit(Callable<T> task) {
        Map<String, String> context = MDC.getCopyOfContextMap();
        log.info("----MDC content:{}",  JSONObject.toJSONString(context));
        return super.submit(() -> {
            // 将父线程的MDC内容传给子线程
                T result = null;
                if (context != null && context.get(LogAspect.requestId) != null && context.get(LogAspect.requestId).length() > 0) {
                    MDC.setContextMap(context);
                } 
                try {
                    result = task.call();
                } finally {
                    try {
                        MDC.clear();
                    } catch (Exception e2) {
                        log.error("mdc clear exception.", e2);
                    }
                }
                return result;
            });
    }
 
}