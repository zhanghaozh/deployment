package com.lenovodevops.deployment.service;


import org.springframework.stereotype.Service;

@Service
public interface DeployService {

    /**
     * 部署完成
     */
    String DONE = "done";

    /** 
     * 检查服务部署结果时的命令有失败返回 
     */
    String CAN_NOT_DETERMINE = "determine fail";

    /** 
     * 存在无实例服务 
     */
    String UNCERTAIN_SERVICE = "service replice 0";
    /** 
     * 未知错误 
     */
    String UNKNOW_ERROR = "unknow error";
    /** 
     * 部署成功 
     */
    String DEPLOYMENT_RLT_OK = "ok";
   
    /** 
     * 部署失败 命令执行失败 
     */
    String EXEC_ERROR = "error";
    /** 
     * 正在部署中 
     */
    String DEPLOYING = "deploying";
    /** 
     * 参数格式错误 
     */
    String PARAM_ERROR = "param error";

    /** 
     * 部署准备失败 
     */
    String PRE_ERROR = "before deployment error";

    /**
     * 缺失服务
     */
    String LACK_SERVICE = "lack service";

    /**
     * 部署次数
     */
    Integer DEPLOYMENT_TIME = 2;

    
    /**
     * 部署参数块最少值
     */
    Integer PARAM_MIN_LENGTH = 4;
    
    /**
     * 部署服务 并把部署结果写入消息队列 如果部署失败回滚
     * 相同参数多次调用时：第一次调用返回部署中，之后一段时间内调用返回部署状态（部署中、部署成功或部署失败）
     * @author 满显凡
     * @param param
     */
    String deployImages(String param);

}
