package com.lenovodevops.deployment.common.schedule;

import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.enums.ResponseStatus;
import com.lenovodevops.deployment.common.util.CacheManagerUtils;
import com.lenovodevops.deployment.model.exception.OverlapsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Component
public class ListContentInit {

    private static final Logger logger = LogManager.getLogger(ListContentInit.class);

    public static final String KEY_ID = "ListKeyID";

    CacheManagerUtils cacheManagerUtils = CacheManagerUtils.getInstance();

    @PostConstruct
    public void init() throws IOException, OverlapsException {
        putContentInCache();
    }

    public void putContentInCache() throws IOException, OverlapsException{
        BufferedReader reader = null;
        try {
            File file = new File(ConfigConsts.LIST_FILE_PATH);
            reader = new BufferedReader(new FileReader(file));
            StringBuffer sb = new StringBuffer();
            String tempString = null;
            int itemNums = 0;
            Set<String> set = new HashSet<>();
            while ((tempString = reader.readLine()) != null) {
                String[] temp = tempString.split("\\|");
                if(tempString.charAt(tempString.length() - 1) == '|'){
                    sb.append(tempString).append(' ').append(',');
                }else {
                    sb.append(tempString).append(',');
                }
                set.add(temp[0]);
                itemNums += 1;
            }
            if (set.size() != itemNums){
                logger.error(ResponseStatus.SERVICENAME_OVERLAPS.getMsg());
                throw new OverlapsException(ResponseStatus.SERVICENAME_OVERLAPS);
            }
            sb.deleteCharAt(sb.length() - 1);
            cacheManagerUtils.put(KEY_ID, sb.toString());
        }catch (IOException e){
            logger.error(e);
            throw e;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

}
