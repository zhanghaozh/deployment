package com.lenovodevops.deployment.controller;

import java.io.IOException;

import javax.validation.Valid;

import com.lenovodevops.deployment.annotation.Log;
import com.lenovodevops.deployment.model.ao.RestartServiceAO;
import com.lenovodevops.deployment.model.exception.CommandFailException;
import com.lenovodevops.deployment.model.exception.EnvironmentException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;
import com.lenovodevops.deployment.model.vo.ListEnvironmentsVO;
import com.lenovodevops.deployment.model.vo.ListServicesByEnvironmentsVO;
import com.lenovodevops.deployment.model.vo.RestartServiceVO;
import com.lenovodevops.deployment.service.DockerStatusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DockerStatusController {
    
    @Autowired
    @Qualifier("DockerStatusSericeImpl")
    private DockerStatusService dockerStatusService;

    /**
     * 获取所有环境名
     * @author 满显凡
     * @return
     */
    @Log
    @GetMapping("/deploy/environments")
    ListEnvironmentsVO listEnvironments() {
        return dockerStatusService.listEnvironments();
    } 

    /**
     * 获取指定环境下的服务名
     * @author 满显凡
     * @param environmentName
     * @return
     * @throws IOException
     * @throws RemoteExecuteException
     * @throws CommandFailException
     * @throws EnvironmentException
     */
    @Log
    @GetMapping("/deploy/services")
    ListServicesByEnvironmentsVO listServicesByEnvironments(@RequestParam(required = true) String environmentName) throws CommandFailException, RemoteExecuteException, IOException, EnvironmentException {
        return dockerStatusService.listServicesByEnvironments(environmentName);
    }

    /**
     * 重启指定服务
     * @author 满显凡
     * @param restartServiceRQB
     * @return
     * @throws EnvironmentException
     */
    @Log
    @PostMapping("/deploy/restart_service")
    RestartServiceVO restartService(@RequestBody @Valid RestartServiceAO restartServiceAO) throws CommandFailException, RemoteExecuteException, IOException, EnvironmentException {
        return dockerStatusService.restartService(restartServiceAO);
    }

}
