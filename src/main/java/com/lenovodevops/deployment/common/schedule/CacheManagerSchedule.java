package com.lenovodevops.deployment.common.schedule;

import com.lenovodevops.deployment.common.util.CacheManagerUtils;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CacheManagerSchedule {
    
    /**
     * @author 吴迪
     * @date 2021/5/24
     * @explaination 自定义定时器传入过期时间（可设置线程数）
     */
    @Scheduled(fixedRate = 1000L * 60 * 60 * 24)
    public void timeExecutor() {
        CacheManagerUtils cacheManagerUtils = CacheManagerUtils.getInstance();
        cacheManagerUtils.checkAndRemove();
    }

}
