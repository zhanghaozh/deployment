package com.lenovodevops.deployment.common.util;

import java.util.regex.Pattern;

/**
 * 判断是否为合法ip
 * @author 满显凡
 */
public class IPUtils {

    final public static String ipPattern = "((?:(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d))";
    
    public static boolean isIp(String ip) {
        return Pattern.matches(ipPattern, ip);
    }
}