package com.lenovodevops.deployment.model.exception;

import com.lenovodevops.deployment.common.enums.ResponseStatus;

/**
 * 通过jsch向服务器发送命令时，若不成功则抛出本异常
 * @author 满显凡
 */
public class RemoteExecuteException extends SystemException {
    
    public RemoteExecuteException(ResponseStatus responseStatus, String msg) {
        super(responseStatus.getCode(), responseStatus.getMsg() + "," + msg);
    }

    public RemoteExecuteException(String msg) {
        super(msg);
    }

}