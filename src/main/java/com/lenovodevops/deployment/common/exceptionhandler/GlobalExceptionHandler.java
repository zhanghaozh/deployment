package com.lenovodevops.deployment.common.exceptionhandler;

import java.io.IOException;
import java.util.List;

import com.lenovodevops.deployment.common.enums.ResponseStatus;
import com.lenovodevops.deployment.model.exception.SystemException;
import com.lenovodevops.deployment.model.vo.ResponseVO;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {
    
    /**
     * 处理业务异常
     * @author 满显凡
     * @param response
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(SystemException.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.ACCEPTED)
    ResponseVO<String> systemException(SystemException e) {
        return new ResponseVO<String>(e.getCode(), e.getMessage(), null);
    } 

    
    /**
     * 处理参数异常
     * @author 满显凡
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseVO<String> methodArgumentNotValidExceptionSystemException(MethodArgumentNotValidException e) {
        // 获取异常信息
        BindingResult exceptions = e.getBindingResult();
        // 判断异常中是否有错误信息，如果存在就使用异常中的消息，否则使用默认消息
        if (exceptions.hasErrors()) {
            List<ObjectError> errors = exceptions.getAllErrors();
            if (!errors.isEmpty()) {
                // 这里列出了全部错误参数，按正常逻辑，只需要第一条错误即可
                FieldError fieldError = (FieldError) errors.get(0);
                return new ResponseVO<String>(ResponseStatus.PARAM_ILLEGAL.getCode(), fieldError.getDefaultMessage(), null);
            }
        }
        return new ResponseVO<String>(ResponseStatus.PARAM_ILLEGAL.getCode(), ResponseStatus.PARAM_ILLEGAL.getMsg(), null);
    }

    /**
     * 处理请求体缺失异常
     * @author 满显凡
     * @return
     */
    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseVO<String> httpMessageNotReadableException() {
        return new ResponseVO<String>(ResponseStatus.REQUESTBODY_NULL.getCode(), ResponseStatus.REQUESTBODY_NULL.getMsg(), null);
    }

    /**
     * 处理文件读取异常
     * @author 满显凡
     * @return
     */
    @ResponseBody
    @ExceptionHandler(IOException.class)
    @org.springframework.web.bind.annotation.ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<ResponseVO<String>> iOException(IOException e) {
        if (e.getMessage() != null) {
            return new ResponseEntity<ResponseVO<String>>(new ResponseVO<String>(ResponseStatus.FILE_IOEXPECTION.getCode(), e.getMessage(), null), HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<ResponseVO<String>>(new ResponseVO<String>(ResponseStatus.FILE_IOEXPECTION.getCode(), e.getMessage(), null), HttpStatus.NOT_FOUND);
        }
        
    }

}
