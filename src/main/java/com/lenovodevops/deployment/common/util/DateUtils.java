package com.lenovodevops.deployment.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    
    /**
     * 获取当前时间 格式yyyy-MM-dd-HH-mm-ss
     * @author 满显凡
     * @return
     */
    public static String getCurrentTimeForID() {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date date = new Date();
        return f.format(date);
    }

    /**
     * 获取指定时间格式 格式yyyy-MM-dd-HH-mm-ss
     * @param time
     * @return
     */
    public static String getTime(Long time) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date date = new Date(time);
        return f.format(date);
    }
}
