package com.lenovodevops.deployment.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.util.FileUtils;
import com.lenovodevops.deployment.model.exception.ConfigFileException;


/**
 * 读取、存储环境配置信息（EnvironmentConfig）的类
 */
public class Medis {

    /**
     * 环境名-环境信息映射
     */
    private static Map<String, EnvironmentConfig> environments = new LinkedHashMap<String, EnvironmentConfig>();

    /** 
     * environments 初始化错误异常信息 
     */
    final private static String configFileUncomplate = "environments.config line: %s error";

    /**
     * 初始化读取存储环境信息的文件
     * @author 满显凡
     */
    static {
        List<String> datas = FileUtils.readFile(ConfigConsts.ENVIRONMENTS_CONFIG_PATH_NAME); 
        if (datas != null) {
            int i = 0;
            for (String data: datas) {
                try {
                    ++i;
                    if (data.length() == 0) {
                        continue;
                    }
                    String[] p = data.split("\\|");
                    if (p.length != 2 && p[1].length() == 0) {
                        throw new ConfigFileException(String.format(Medis.configFileUncomplate, Integer.toString(i)));
                    }
                    EnvironmentConfig ec = new EnvironmentConfig(p[0], p[1]);
                    Medis.environments.put(ec.getName(), ec);
                } catch (ConfigFileException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 获取指定环境的环境配置信息
     * @author 满显凡
     * @param name
     * @return
     */
    public static EnvironmentConfig getEnvironmentConfigByName(String name) {
        return environments.get(name);
    }
    
    /**
     * 判断某环境是否存在
     * @author 满显凡
     * @param name
     * @return
     */
    public static Boolean containsEnvironment(String name) {
        return environments.containsKey(name);
    }

    /**
     * 获取已存在的全部环境名
     * @author 满显凡
     * @return
     */
    public static List<String> getAllEnvironmentsName() {
        List<String> names = new ArrayList<String>();
        for (Entry<String,EnvironmentConfig> entry: environments.entrySet()) {
            names.add(entry.getKey());
        }
        return names;
    }

    /**
     * 插入新的环境信息
     * @author 满显凡
     * @param ec
     */
    public static void InsertEnvironment(EnvironmentConfig ec) throws IOException {
        environments.put(ec.getName(), ec);
        saveConfig();
    }

    /**
     * 将内存中的环境信息持久化
     * @author 满显凡
     */
    private static void saveConfig() throws IOException {
        StringBuffer sb = new StringBuffer();
        for (Entry<String,EnvironmentConfig> entry: environments.entrySet()) {
            sb.append(entry.getValue().toString());
            sb.append("\n");
        }
        FileUtils.saveAsFile(sb.toString(), ConfigConsts.ENVIRONMENTS_CONFIG_PATH_NAME, false);
    }

}