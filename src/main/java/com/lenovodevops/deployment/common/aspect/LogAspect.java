package com.lenovodevops.deployment.common.aspect;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.lenovodevops.deployment.common.util.RequestUtils;
import com.lenovodevops.deployment.model.entity.LogEntity;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 满显凡
 */
@Aspect
@Component
@Slf4j
public class LogAspect {

    final public static String requestId = "id"; 

    @Pointcut("@annotation(com.lenovodevops.deployment.annotation.Log)")
    public void logPointCut(){}

    @Around("logPointCut()")
    public Object aroundCut(ProceedingJoinPoint joinPoint) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        LogEntity logEntity = new LogEntity();

        // 记录下请求内容
        logEntity.setType(request.getMethod());
        logEntity.setUrl(request.getRequestURL().toString());
        logEntity.setIp(RequestUtils.getIpAddr(request));
        logEntity.setMethod(joinPoint.getSignature().getDeclaringTypeName() + "." + method.getName());

        // 只记录post方法 传json格式的数据
        if(HttpMethod.POST.matches((request.getMethod()))){
            //方法 1 请求的方法参数值 JSON 格式 null不显示
            if (joinPoint.getArgs().length > 0) {
                for (Object o : joinPoint.getArgs()) {
                    if (o instanceof HttpServletRequest || o instanceof HttpServletResponse) {
                        continue;
                    }
                    // logger.info("请求参数 :" + JSON.toJSONString(o));
                    logEntity.setParam(JSON.toJSONString(o));
                }
            }
        }else {
            //请求的方法参数值 兼容fromDate格式和JSON格式
            Object[] args = joinPoint.getArgs();
            // 请求的方法参数名称 显示所有字段 值为null也显示该字段
            LocalVariableTableParameterNameDiscoverer u = new LocalVariableTableParameterNameDiscoverer();
            String[] paramNames = u.getParameterNames(method);
            if (args != null && paramNames != null) {
                StringBuffer params = new StringBuffer();
                for (int i = 0; i < args.length; i++) {
                    params.append(paramNames[i])
                        .append("=")
                        .append(args[i])
                        .append("&");
                }
                if (params.length() > 0) {
                    params.replace(params.length() - 1, params.length(), "");
                }
                // logger.info("请求参数 : "+params);
                logEntity.setParam(params.toString());
            }
        }
        String id = Long.toString(System.currentTimeMillis());
        MDC.put(requestId, id);
        log.info(String.format("接收请求-%s", JSON.toJSONString(logEntity)));
        try {
            Object result = joinPoint.proceed();
            logEntity.setResult(JSON.toJSONString(result));
            log.info(String.format("请求处理完成-%s", JSON.toJSONString(logEntity)));
            return result;
        } catch(Exception e) {
            logEntity.setResult(e.getMessage());
            log.info(String.format("请求处理完成-存在异常-%s", JSON.toJSONString(logEntity)));
            throw e;
        } finally {
            MDC.remove(requestId);
        }

    }

}
