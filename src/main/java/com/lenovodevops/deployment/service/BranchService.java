package com.lenovodevops.deployment.service;

import com.lenovodevops.deployment.model.exception.GitFailException;
import org.springframework.stereotype.Service;


/**
 * @author 吴迪
 * @date 2021/6/1
 * @explaination 获取git地址下的所有分支服务
 * */
@Service
public interface BranchService {
    String getRemoteBranches(String gitParam) throws Exception;
}
