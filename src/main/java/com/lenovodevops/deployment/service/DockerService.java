package com.lenovodevops.deployment.service;

import java.io.IOException;

import com.lenovodevops.deployment.db.EnvironmentConfig;
import com.lenovodevops.deployment.model.bo.CommandRltBO;
import com.lenovodevops.deployment.model.exception.FileUploadException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;

import org.springframework.stereotype.Service;

@Service
public interface DockerService {
    
    /**
     * 上传文件到指定环境
     * @author 满显凡
     * @param environmentConfig
     * @param yamlFileName
     * @throws FileUploadException
     */
    void uploadFile(EnvironmentConfig environmentConfig, String yamlFileName) throws FileUploadException;

    /**
     * 在指定主机发布指部署命令
     * @author 满显凡
     * @param en
     * @param yamlFileName
     * @param stackName
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    CommandRltBO deployStack(EnvironmentConfig en, String yamlFileName, String stackName) throws RemoteExecuteException, IOException;

    /**
     * 移除指定服务
     * @author 满显凡
     * @param en 环境信息
     * @param stackName 堆栈名
     * @param serviceName 服务名
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    CommandRltBO rmService(EnvironmentConfig en, String stackName, String serviceName) throws RemoteExecuteException, IOException;

    /**
     * 列出某环境下的全部服务
     * @author 满显凡
     * @param en
     * @param stackName
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    CommandRltBO listServices(EnvironmentConfig en, String stackName) throws RemoteExecuteException, IOException;

    /**
     * 重启指定服务
     * @param en
     * @param serviceName
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    CommandRltBO restartService(EnvironmentConfig en, String serviceName) throws RemoteExecuteException, IOException;
    /**
     * 移除多个指定服务
     * @author 吴迪
     * @param en 环境信息
     * @paramm stackName 堆栈名
     * @paramm serviceNameList 服务名列表
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    CommandRltBO rmServiceList(EnvironmentConfig en, String stackName, String[] serviceNameList) throws RemoteExecuteException, IOException;

    /**
     * 移除某个环境下的所有服务
     * @author 吴迪
     * @param en 环境信息
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    CommandRltBO rmStack(EnvironmentConfig en, String stackName) throws RemoteExecuteException, IOException;

    /**
     * 移除某环境过期的文件
     * @param en
     * @param path 被检查的
     * @param time
     * @return
     */
    void removeExpireFile(EnvironmentConfig en, String path, String file, Integer time);
}
