package com.lenovodevops.deployment.model.ao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RmServiceListAO {
    
    @NotBlank(message = "environment can not blank")
    private String environment;
    @NotEmpty(message = "services can not empty")
    private String[] services;
}
