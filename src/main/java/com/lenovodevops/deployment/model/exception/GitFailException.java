package com.lenovodevops.deployment.model.exception;

import com.lenovodevops.deployment.common.enums.ResponseStatus;

public class GitFailException extends SystemException{

    public GitFailException(ResponseStatus responseStatus){
        super(responseStatus.getCode(), responseStatus.getMsg());
    }

    public GitFailException(String msg){
        super(msg);
    }
}
