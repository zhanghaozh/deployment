package com.lenovodevops.deployment.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.util.DockerCommandUtils;
import com.lenovodevops.deployment.common.util.LinuxCommandUtils;
import com.lenovodevops.deployment.common.util.SSHUtils;
import com.lenovodevops.deployment.db.EnvironmentConfig;
import com.lenovodevops.deployment.model.bo.CommandRltBO;
import com.lenovodevops.deployment.model.exception.FileUploadException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;
import com.lenovodevops.deployment.service.DockerService;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 用于跟docker集群通信
 */
@Slf4j
@Service("DockerServiceImpl")
public class DockerServiceImpl implements DockerService {
    
    /**
     * 上传文件到指定环境
     * @author 满显凡
     * @param environmentConfig
     * @param yamlFileName
     * @throws FileUploadException
     */
    @Override
    public void uploadFile(EnvironmentConfig environmentConfig, String yamlFileName) throws FileUploadException {
        log.info(String.format("上传文件 %s 到 %s", yamlFileName, environmentConfig.getIp()));
        SSHUtils.uploadFile(environmentConfig, ConfigConsts.YAML_FILE_PATH, yamlFileName, ConfigConsts.YAML_FILE_TARGET_PATH);
    }

    /**
     * 在指定主机发布指部署命令
     * @author 满显凡
     * @param en
     * @param yamlFileName
     * @param stackName
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    @Override
    public CommandRltBO deployStack(EnvironmentConfig en, String yamlFileName, String stackName) throws RemoteExecuteException, IOException {
        String command = DockerCommandUtils.deployStack(ConfigConsts.YAML_FILE_TARGET_PATH + yamlFileName, stackName);
        log.info(String.format("上传命令 %s 到 %s", command, en.getIp()));
        List<String> rlt = SSHUtils.remoteExecute(command, en);
        return this.getCommandRltBO(rlt);
    }

    /**
     * 移除某个环境下的所有服务
     * @author 吴迪
     * @param en 环境信息
     * @param stackName 栈名
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    @Override
    public CommandRltBO rmStack(EnvironmentConfig en, String stackName) throws RemoteExecuteException, IOException {
        String command = DockerCommandUtils.rmStack(stackName);
        log.info(String.format("上传命令 %s 到 %s", command, en.getIp()));
        List<String> rlt = SSHUtils.remoteExecute(command, en);
        return this.getCommandRltBO(rlt);
    }

    /**
     * 移除指定服务
     * @author 满显凡
     * @param en 环境信息
     * @param stackName 堆栈名
     * @param serviceName 服务名
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    @Override
    public CommandRltBO rmService(EnvironmentConfig en, String stackName, String serviceName) throws RemoteExecuteException, IOException {
        String command = DockerCommandUtils.rmService(stackName, serviceName);
        log.info(String.format("上传命令 %s 到 %s", command, en.getIp()));
        List<String> rlt = SSHUtils.remoteExecute(command, en);
        return this.getCommandRltBO(rlt);
    }

    /**
     * 列出某环境下的全部服务
     * @author 满显凡
     * @param en
     * @param stackName
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    @Override
    public CommandRltBO listServices(EnvironmentConfig en, String stackName) throws RemoteExecuteException, IOException {
        String command = DockerCommandUtils.dockerStackServices(stackName);
        log.info(String.format("上传命令 %s 到 %s", command, en.getIp()));
        List<String> rlt = SSHUtils.remoteExecute(command, en);
        return this.getCommandRltBO(rlt);
    }

    /**
     * 重启指定服务
     * @param en
     * @param serviceName
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    @Override
    public CommandRltBO restartService(EnvironmentConfig en, String serviceName) throws RemoteExecuteException, IOException {
        String command = DockerCommandUtils.restartService(serviceName);
        log.info(String.format("上传命令 %s 到 %s", command, en.getIp()));
        List<String> rlt = SSHUtils.remoteExecute(command, en);
        return this.getCommandRltBO(rlt);
    }

    /**
     * 移除多个指定服务
     * @author 吴迪
     * @param en 环境信息
     * @paramm stackName 堆栈名
     * @paramm serviceNameList 服务名列表
     * @return
     * @throws RemoteExecuteException
     * @throws IOException
     */
    @Override
    public CommandRltBO rmServiceList(EnvironmentConfig en, String stackName, String[] serviceNameList) throws RemoteExecuteException, IOException {
        String command = DockerCommandUtils.rmServiceList(stackName, serviceNameList);
        log.info(String.format("上传命令 %s 到 %s", command, en.getIp()));
        List<String> rlt = SSHUtils.remoteExecute(command, en);
        return this.getCommandRltBO(rlt);
    }

    /**
     * @author 满显凡
     */
    @Override
    public void removeExpireFile(EnvironmentConfig en, String path, String file, Integer time) {
        log.info("开始清除过期yaml文件");
        String command = LinuxCommandUtils.removeExpireFile(path, file, time);
        log.info(String.format("上传命令 %s 到 %s", command, en.getIp()));
        try {
            List<String> rlt = SSHUtils.remoteExecute(command, en);
            CommandRltBO commandRltBO = this.getCommandRltBO(rlt);
            if (commandRltBO.isSuccess()) {
                log.info("清除过期yaml成功");
            } else {
                log.info("清除过期yaml失败 " + commandRltBO.getErrString());
            }
        } catch(IOException|RemoteExecuteException e) {
            log.info("清除过期yaml失败", e);
        }
    }

    /**
     * @author 满显凡
     * @param rlt
     * @return
     */
    private CommandRltBO getCommandRltBO(List<String> rlt) {
        CommandRltBO commandRltBO = new CommandRltBO();
        Integer execLine = rlt.indexOf(SSHUtils.execExtLine);
        List<String> ok = new ArrayList<String>();
        for (int i = 0; i < execLine; ++i) {
            ok.add(rlt.get(i));
        }
        commandRltBO.setOk(ok);
        if (rlt.indexOf(SSHUtils.execExtLine) != rlt.size() - 1) {
            commandRltBO.setSuccess(false);
            List<String> err = new ArrayList<String>();
            for (int i = execLine + 1; i < rlt.size(); ++i) {
                err.add(rlt.get(i));
            }
            commandRltBO.setErr(err);
        } else {
            commandRltBO.setSuccess(true);
        }
        return commandRltBO;
    }

    

}
