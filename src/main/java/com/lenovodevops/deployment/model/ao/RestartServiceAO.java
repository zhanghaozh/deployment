package com.lenovodevops.deployment.model.ao;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestartServiceAO {

    @NotBlank(message = "serviceNotBlank")
    private String service;
    @NotBlank(message = "environmentNotBlank")
    private String environment;

}
