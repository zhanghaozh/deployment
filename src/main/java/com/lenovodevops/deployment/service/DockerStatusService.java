package com.lenovodevops.deployment.service;

import java.io.IOException;

import com.lenovodevops.deployment.model.ao.RestartServiceAO;
import com.lenovodevops.deployment.model.ao.RmServiceListAO;
import com.lenovodevops.deployment.model.exception.CommandFailException;
import com.lenovodevops.deployment.model.exception.EnvironmentException;
import com.lenovodevops.deployment.model.exception.RemoteExecuteException;
import com.lenovodevops.deployment.model.vo.ListEnvironmentsVO;
import com.lenovodevops.deployment.model.vo.ListServicesByEnvironmentsVO;
import com.lenovodevops.deployment.model.vo.RestartServiceVO;
import com.lenovodevops.deployment.model.vo.RmServiceListVO;

import org.springframework.stereotype.Service;

@Service
public interface DockerStatusService {
    
    /**
     * 返回所有环境名
     * @return
     */
    ListEnvironmentsVO listEnvironments();

    /**
     * 返回指定环境下的service名字
     * @author 满显凡
     */
    ListServicesByEnvironmentsVO listServicesByEnvironments(String environmentName) throws CommandFailException, RemoteExecuteException, IOException, EnvironmentException;

    /**
     * 重启指定服务 
     * @author 满显凡
     * @param restartServiceRQB
     * @return
     */
    RestartServiceVO restartService(RestartServiceAO restartServiceRQB) throws CommandFailException, RemoteExecuteException, IOException, EnvironmentException;
    
    /**
     * 删除一个或多个服务
     * @author 满显凡
     * @param RmServiceListAO
     * @return
     */
    RmServiceListVO rmServiceList(RmServiceListAO RmServiceListAO) throws RemoteExecuteException, IOException, EnvironmentException, CommandFailException;

    /**
     * @author 吴迪
     * @date 2021/6/7
     * @explaination 删除指定环境下的服务
     * @return 已移除的服务名列表
     */
    RmServiceListVO rmServicesInStack(String environmentName) throws CommandFailException, IOException, EnvironmentException, RemoteExecuteException;
}
