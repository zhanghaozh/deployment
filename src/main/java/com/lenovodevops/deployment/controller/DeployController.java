package com.lenovodevops.deployment.controller;

import com.lenovodevops.deployment.annotation.Log;
import com.lenovodevops.deployment.service.DeployService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeployController {

    @Autowired
    @Qualifier("DeployServiceInteractive")
    private DeployService deployService;

    /**
     * 依据参数部署服务
     * param例 {environment}|{ip}|{name0:tag0}|{name1:tag1}|...
     * 相同参数多次调用时：第一次调用返回部署中，之后一段时间内调用返回部署状态（部署中、部署成功或部署失败）
     * @author 满显凡
     * @param deployParam
     * @return
     */
    @Log
    @PostMapping("/deploy")
    String deploy(@RequestBody String param) {
        return deployService.deployImages(param);
    }

}
