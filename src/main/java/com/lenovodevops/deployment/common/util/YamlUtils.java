package com.lenovodevops.deployment.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.lenovodevops.deployment.common.consts.ConfigConsts;
import com.lenovodevops.deployment.common.consts.DockerConsts;
import com.lenovodevops.deployment.db.YamlMemory;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

public class YamlUtils {

    /**
     * 读取yaml文件
     * @author 满显凡
     * @param customYaml
     * @return
     * @throws FileNotFoundException
     */
    public static Map<Object, Object> loadYaml(String customYaml) throws FileNotFoundException {
        FileInputStream fileInputStream = null;
        try {
            //实例化解析器
            Yaml yaml = new Yaml();
            //配置文件地址
            File file = new File(customYaml);
            fileInputStream = new FileInputStream(file);
            //装载的对象，这里使用Map, 当然也可使用自己写的对象
            Map<Object, Object> map = yaml.loadAs(fileInputStream, Map.class);
            return map;
        }catch(FileNotFoundException e) {
            throw e;
        }finally {
            try {
                if (fileInputStream!=null) {
                    fileInputStream.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    
     /**
     * 将map写入yaml文件 使用snakeyaml包
     * @param map
     * @throws IOException
     */
    public static void createYaml(Map<Object, Object> map, String yamlName) throws IOException {
        DumperOptions dumperOptions = new DumperOptions();
        dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Yaml yaml = new Yaml(dumperOptions);
        File dumpFile = new File(ConfigConsts.YAML_FILE_PATH + yamlName);
        FileWriter fileWriter = new FileWriter(dumpFile);
        yaml.dump(map, fileWriter);
        fileWriter.close();
    }

    /**
     * docker-compse.yml的模板 
     * param %s_0 docker网络名称
     * @author 满显凡
     */
    final public static String yamlTemplate = 
    "{ "                              + 
        "\"version\": '3.0',"         +
        "\"services\": null,"         + 
        "\"networks\": {"             + 
            "\"%s\": {"               +
                "\"external\": true"  +
            "}"                       +
        "}"                           +
    "}"; 
    
    /**
     * docker-compse.yml中services部分的模板 
     * param %s_0 service名称
     * param %s_1 image名称
     * param %s_2 network名称
     * @author 满显凡
     */
    final public static String serviceTemplate =  
    "{"                                             +
        "\"%s\": {"                                 +
            "\"image\": \"%s\","                    +
            "\"networks\": [\"%s\"],"               + 
            "\"deploy\": {"                         +
                "\"replicas\": 1,"                  +
                "\"restart_policy\": {"             +
                    "\"condition\": \"on-failure\"" + 
                "}"                                 + 
            "}"                                     + 
        "}"                                         + 
    "}";

    /**
     * 生成本次部署需要的yaml文件（包含自定义yaml的内容）
     * @author 满显凡
     * @param environment
     * @return
     */
    public static Map<Object, Object> createYamlMap(List<String> images) {
        // 生成docker-compose.yml最外层模板
        String yaml = String.format(YamlUtils.yamlTemplate, DockerConsts.DOCKER_NETWORK);
        JSONObject json;
        json = JSON.parseObject(yaml, Feature.OrderedField);
        Map yamlMap = JSON.toJavaObject(json, LinkedHashMap.class);
        // 存储services的map
        Map<Object,Object> services = new LinkedHashMap<>();
        Map customYaml = YamlMemory.getCustomYaml();
        for (String image: images) {
            String[] rImage = image.split("/");
            String[] nameAndTag = rImage[rImage.length - 1].split(":");
            String serviceName = nameAndTag[0];
            String serviceYaml = String.format(YamlUtils.serviceTemplate, serviceName, image, DockerConsts.DOCKER_NETWORK);
            json = JSON.parseObject(serviceYaml, Feature.OrderedField);
            Map service = JSON.toJavaObject(json, LinkedHashMap.class);
            
            // 获取模板
            Map customService = (Map)customYaml.get(serviceName);
            if (customService != null) {
                // 添加模板内容 相同时使用默认内容
                RecursionMergeBiFunction.recursionMergeMap((Map)service.get(serviceName), customService);
            }
            services.putAll(service);
        }
        yamlMap.put("services", services);
        return yamlMap;
    }
}