package com.lenovodevops.deployment.common.util;

import java.util.Map;
import java.util.function.BiFunction;

/**
 * 用来递归合并两个map
 * @author 满显凡
 */
public class RecursionMergeBiFunction implements BiFunction<Object, Object, Object> {
    /**
     * 把t合并到u中 当值不为map且重复时 保留u中的值
     * @author 满显凡
     * @param u
     * @param t
     * @return
     */
    public static Map<Object,Object> recursionMergeMap(Map<Object,Object> u, Map<Object,Object> t) {
        t.forEach((key, value) -> u.merge(key, value, new RecursionMergeBiFunction()));
        return u;
    }

    @Override
    public Object apply(Object u, Object t) {
        if (t instanceof Map) {
            // 一开始u t写反了 凎
            return RecursionMergeBiFunction.recursionMergeMap((Map)u, (Map)t); 
        } else {
            return u;   
        }
    }
    
}
