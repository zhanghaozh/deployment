#!/bin/sh
# author manxianfan
file="/root/.ssh/authorized_keys"

# 这里的-f参数判断file是否存在
if [ ! -f ${file} ]; then
 touch ${file}
fi

cat >> ${file} << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCc5UJHNKQf/9DQZ+30gDk+nmfPGFKrSwZl/l/7+Wo0DJgDVSMV8cap3pbSETxmyUvtAcLaRJ+zvQKeEdNI1Ac3T4Y0nJ+GipT39YurYeugYvTKYR+XgOee2eZCDeKi7PNRIh+TOiTRMoji5AjOPOQzMz9MQquag18yqcRkqOBXz7NL4X02dApdIYn3AMid4OV/I+Ic5H6aJlcVynw8ok8kbNq6HWVG5FSvqlaxui/7R2ePD9vbVGiO7w445MeWuyeJsmb9IuV38jRZPGWhn7NO/e9QfwaWsrEWMWvGagKBDVcSLT47OmROCzxM3ESYnblevxLm9guiVUJr64WXgRn3 root@host
EOF
echo "success"
