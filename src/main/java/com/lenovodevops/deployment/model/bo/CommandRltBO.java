package com.lenovodevops.deployment.model.bo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 满显凡
 */
@Getter
@Setter
public class CommandRltBO {

    private boolean success;
    private List<String> ok;
    private List<String> err;

    public String getInfo() {
        StringBuffer sb = new StringBuffer();
        if (this.ok != null && this.ok.size() > 0) {
            for (String s: this.ok) {
                sb.append(s).append("\n");
            }
        } 
        if (this.err != null && this.err.size() > 0) {
            sb.append("---------warning or error---------\n");
            for (String s: this.err) {
                sb.append(s).append("\n");
            }
        }
        if (sb.length() > 0) {
            sb.replace(sb.length() - 1, sb.length(), "");
        }
        return sb.toString();
    }

    public String getOkString() {
        if (this.ok != null && this.ok.size() > 0) {
            StringBuffer okS = new StringBuffer();
            for (String s: this.ok) {
                okS.append(s);
                okS.append("\n");
            }
            okS.replace(okS.length() - 1, okS.length(), "");
            return okS.toString();
        } else {
            return "";
        }
    }

    public String getErrString() {
        if (this.err != null && this.err.size() > 0) {
            StringBuffer errS = new StringBuffer();
            for (String s: this.err) {
                errS.append(s);
                errS.append("\n");
            }
            errS.replace(errS.length() - 1, errS.length(), "");
            return errS.toString();
        } else {
            return "";
        }
    }

}
