package com.lenovodevops.deployment.controller;

import java.io.IOException;

import com.lenovodevops.deployment.annotation.Log;
import com.lenovodevops.deployment.model.exception.OverlapsException;
import com.lenovodevops.deployment.service.ListService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ListController {

    @Autowired
    ListService listService;

    /*
    * @author 吴迪
    * @date 2021/5/24
    * @return 清单内容
    */
    @Log
    @GetMapping("/list")
    public String orderlist() throws IOException, OverlapsException{
        return listService.readLocalCheckList();
    }

}
