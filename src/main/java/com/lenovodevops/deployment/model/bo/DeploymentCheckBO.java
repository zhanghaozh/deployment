package com.lenovodevops.deployment.model.bo;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 满显凡
 */
@Getter
@Setter
@NoArgsConstructor
public class DeploymentCheckBO {
    
    /** 
     * 检查结果为部署成功 
     */
    final public static Integer RLT_OK = 1; 
    /** 
     * 检查结果为有无实例的服务,不确定是否成功
     * uncertains中为无实例的服务
     */
    final public static Integer RLT_UNKNOW =  2;
    /**
     * 检查结果为有不存在的服务
     */
    final public static Integer RLT_DEFECT = 4;
    /** 
     * 检查时出错 
     */
    final public static Integer RLT_ERR = 3;
    private Integer rlt;
    /**
     * command执行结果
     */
    private String info;
    private List<ServiceStatusBO> uncertains;

    public DeploymentCheckBO(Integer rlt) {
        this.rlt = rlt;
        this.uncertains = null;
    }

    public DeploymentCheckBO(Integer rlt, String info) {
        this.rlt = rlt;
        this.info = info;
        this.uncertains = null;
    }

    public String getUncertainsString() {
        StringBuffer sb = new StringBuffer();
        for (ServiceStatusBO s: uncertains) {
            sb.append(s.getService());
            sb.append(" ");
            sb.append(s.getReplicas());
            sb.append(",");
        }
        sb.replace(sb.length() - 1, sb.length(), "");
        return sb.toString();
    }
}
