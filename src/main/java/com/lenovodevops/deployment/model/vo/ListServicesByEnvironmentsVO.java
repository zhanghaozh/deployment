package com.lenovodevops.deployment.model.vo;

import java.util.List;

import com.lenovodevops.deployment.model.bo.ServiceStatusBO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 返回指定环境下的服务名
 * @author 满显凡
 */
@Getter
@Setter
@AllArgsConstructor
public class ListServicesByEnvironmentsVO {
    
    private List<ServiceStatusBO> serviceStatus;

}
